package Device;

import java.util.ArrayList;

public class Device {

    protected boolean isDeviceActive = false;

    protected ArrayList<DevicePart> partsList = new ArrayList<>();

    /**
     * Activate the device so it can be controlled. This has to be called to activated all attached parts.
     */
    public void start() {
        activateDevice();
    }

    private void activateDevice() {
        //start up all parts now that the communicator is active
        for (DevicePart part : partsList) {
            part.activate();
        }
        isDeviceActive = true;
    }


    //it is possible to add parts multiple times, as this could make sense for external services, like 2 infrared sensors

    /**
     * Add multiple parts to the device.
     * @param parts multiple parts, separated by a comma, to add to the device
     */
    public void addPart(DevicePart... parts) {
        for (DevicePart part : parts) {
            addPart(part);
        }
    }

    /**
     * Add a part the the device.
     * @param part the part to add and bind
     */
    protected void addPart(DevicePart part) {
        part.bindPartToDevice(this);
        partsList.add(part);
        if (isDeviceActive) {
            part.activate();
        }
    }


    //remove device dependent info so part can still be repurposed

    /**
     * remove parts from the device. This part can still be used by adding it again to this device or another device.
     * @param parts a list of parts, separated by commas, to remove from the device.
     */
    public void removePart(DevicePart... parts) {
        for (DevicePart part : parts) {
           removePart(part);
        }
    }
    protected void removePart(DevicePart part){
        part.unbindPartFromDevice();
        part.cleanup(isLastPartType(part));
        partsList.remove(part);
    }

    private boolean isLastPartType(DevicePart part) {
        for (DevicePart p : partsList) {
            //check if there is a part in the parts list that is of the same class, but not the same object (reference)
            if (p != part && p.getClass().equals(part.getClass())) {
                return false;
            }
        }
        return true;
    }
}
