package Device;

public abstract class DevicePart {
    private Device device = null;
    private boolean isActive = false;
    void bindPartToDevice(Device device) {
        this.device = device;
    }
    protected void unbindPartFromDevice(){this.device = null;}
    protected abstract void cleanup(boolean isLastPart);
    //call when communicator has been connected to a device and communication is set up.
    protected void activate(){
        isActive = true;
    }
    protected boolean isActive(){
        return isActive;
    }
    protected Device getDevice(DevicePart callingPart){
        if(device == null) {
            System.out.println("error: " + callingPart.getClass().getName() + " is not part of a device.\n" +
                    "Call addPart() first to bind it.");
            System.exit(-1);
        }else if (!isActive){
            System.out.println("error: the device is not yet activated. Call start() on device to activate it.");
        }
        return device;
    }
}
