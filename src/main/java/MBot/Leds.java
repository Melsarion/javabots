package MBot;

public class Leds extends MbotPart {
    /**
     * A class to control the two LEDs on top of the board of the mBot.
     */
    public Leds() {
        setPortNumber(-1);
    }


    private void setLeds(boolean leftLed, boolean rightLed, int R, int G, int B){
        if(R>255 ||G > 255 ||B >255){
            throw new IllegalArgumentException("invalid RGB value. Value should be between 0 and 255.");
        }else{
            getCommunicator(this).setLedColor(leftLed,rightLed,R,G,B);
        }
    }

    /**
     * Set the left LED on the mBot board with an RGB value.
     * @param R the color red between 0 and 255
     * @param G the color green between 0 and 255
     * @param B the color bllue between 0 and 255
     */
    public void setLeftLed(int R, int G, int B){
        setLeds(true,false,R,G,B);
    }
    /**
     * Set the right LED on the mBot board with an RGB value.
     * @param R the color red between 0 and 255
     * @param G the color green between 0 and 255
     * @param B the color bllue between 0 and 255
     */
    public void setRightLed(int R, int G, int B){
        setLeds(false,true,R,G,B);
    }
    /**
     * Set both LEDs on the mBot board with an RGB value.
     * @param R the color red between 0 and 255
     * @param G the color green between 0 and 255
     * @param B the color bllue between 0 and 255
     */
    public void setBothLeds(int R, int G, int B){
        setLeds(true, true, R, G ,B);
    }

    @Override
    protected void cleanup(boolean isLastPart) {

    }
}
