package MBot;

import Device.DevicePart;
import Device.Device;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MbotDevice extends Device implements MbotDeviceInterface{
    private MbotWirelessCommunication communicator;
    private final Executor listenerExecutor = Executors.newSingleThreadExecutor();

    /**
     * A base mBot device. Add parts to control it.
     */
    public MbotDevice() {
        this.communicator = new MbotWirelessCommunication(this);
    }

    /**
     * Start the mBot device to start controlling it.
     */
    @Override
    public void start() {
        communicator.start();
        super.start();
        //set mbot to default values for motor and light, as the mBot saves these when powered off.
        communicator.setDCMotor(0,Motor.LEFT);
        communicator.setDCMotor(0,Motor.RIGHT);
        communicator.setLedColor(true,true,0,0,0);
    }

    protected MbotWirelessCommunication getCommunicator(){
        return communicator;
    }

    public void onDeviceButtonPress() {
        for(DevicePart part : partsList){
            if(part instanceof Button){
                listenerExecutor.execute(((Button) part)::onButtonPress);
            }
        }
    }
    public void onDeviceButtonRelease() {
        for(DevicePart part : partsList){
            if(part instanceof Button){
                listenerExecutor.execute(((Button) part)::onButtonRelease);
            }
        }
    }
}
