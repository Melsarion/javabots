package MBot;

import Device.DevicePart;

abstract class MbotPart extends DevicePart {
    private int portNumber;

    @Override
     protected void unbindPartFromDevice() {
        super.unbindPartFromDevice();
        setPortNumber(-1);
    }

    protected MbotWirelessCommunication getCommunicator(DevicePart callingPart) {
        return ((MbotDevice) super.getDevice(callingPart)).getCommunicator();
    }

    /**
     * Get the port the part is plugged into. Will return -1 if no port is specified or if the part does not need to be plugged into a port of the mBot.
     * @return the port number as shown on the mBot.
     */
    int getPortNumber(){
        return  portNumber;
    }
    void setPortNumber(int portNumber){
        this.portNumber = portNumber;
    }
}
