package MBot.Listeners;

public interface ButtonListener {
    default void onButtonPress(){};
    default void onButtonRelease(){};
}
