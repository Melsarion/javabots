package MBot;

import java.util.concurrent.ExecutionException;

public class UltrasonicSensor extends MbotPart {
    /**
     * The class to interact with the ultrasonic sensor that can be plugged into the mBot
     * @param portNumber the port number that the ultrasonic sensor is plugged into
     */
    public UltrasonicSensor(int portNumber) {
        setPortNumber(portNumber);
    }

    /**
     * read the current value of the ultrasonic sensor
     * @return the distance given by the sensor
     */
    public float readUltrasonicSensor(){
        try {
            return getCommunicator(this).readUltrasonicSensorAsync(getPortNumber()).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    protected void cleanup(boolean isLastPart) {

    }
}
