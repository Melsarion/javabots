package MBot;

public class Buzzer extends MbotPart {
    /**
     * The buzzer on the mBot.
     */
    public Buzzer() {
        setPortNumber(-1);
    }

    @Override
    protected void cleanup(boolean isLastPart) {

    }

    /**
     * Let the buzzer make a sound.
     * @param toneInHz the tone to produce in Hz. In standard tuning 440 is A4.
     * @param time the time to hold the tone.
     */
    void setBuzzer(int toneInHz, int time){
        if (time > 0 && time < Short.MAX_VALUE){
            getCommunicator(this).setBuzzerTone(toneInHz, time);
        }else{
            throw new IllegalArgumentException("time has to be between 0 and " + Short.MAX_VALUE);
        }
    }
    void setBuzzer(int toneInHz){
        getCommunicator(this).setBuzzerTone(toneInHz);
    }
}
