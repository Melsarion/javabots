package MBot;

import Device.Device;

interface MbotDeviceInterface {
    void onDeviceButtonPress();

    void onDeviceButtonRelease();
}
