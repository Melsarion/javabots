package MBot;

public class Motors extends MbotPart {
    /**
     * The class to interact with both the motors attached to the mBot.
     */
    public Motors() {
        setPortNumber(-1);
    }

    /**
     * Set the speed of the specified motor.
     * @param side which motor to change (Motor.LEFT or Motor.RIGHT)
     * @param speed the speed to set the motor to, between -255 and 255.
     */
    public void setMotor(Motor side, int speed){
        getCommunicator(this).setDCMotor(speed,side);
    }
    /**
     * Set both motors to the same specified speed
     * @param speed the speed to set the motors to, between -255 and 255.
     */
    public void setMotors(int speed){
        getCommunicator(this).setDCMotor(speed, Motor.LEFT);
        getCommunicator(this).setDCMotor(speed, Motor.RIGHT);
    }
    @Override
    protected void cleanup(boolean isLastPart) {

    }
}
