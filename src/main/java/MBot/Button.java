package MBot;

import MBot.Listeners.ButtonListener;

import java.util.ArrayList;

public class Button extends MbotPart {
    private ArrayList<ButtonListener> listeners = new ArrayList<>();

    /**
     * The Button of the mBot.
     */
    public Button() {
        setPortNumber(-1);
    }

    void onButtonPress(){
        for(ButtonListener listener : listeners){
            listener.onButtonPress();
        }
    }
    void onButtonRelease(){
        for(ButtonListener listener : listeners){
            listener.onButtonRelease();
        }
    }

    /**
     * Add a listener that will receive updates about button presses.
     *
     * @param listener the button listener subscribing to updates
     */
    public void addButtonListener(ButtonListener listener){
        listeners.add(listener);
    }

    /**
     * Remove a listener. This listener will not receive updates from this part anymore.
     *
     * @param listener the button listener to remove
     */
    public void removeButtonListener(ButtonListener listener){
        listeners.remove(listener);
    }

    protected void cleanup(boolean isLastPart) {

    }
}
