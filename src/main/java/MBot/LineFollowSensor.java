package MBot;

import java.util.concurrent.ExecutionException;

public class LineFollowSensor extends MbotPart{

    /**
     * A class to read the line follower of the mBot
     * @param portNumber the port number that the sensor is attached to on the mBot
     */
    public LineFollowSensor(int portNumber){
        setPortNumber(portNumber);
    }

    /**
     * Read the current state of the line follow sensor.
     * @return a LineFollowerState object with the current state of the sensor
     */
    public LineFollowerState readLineFollowSensor(){
        try {
            return getCommunicator(this).readLineFollowSensorAsync(getPortNumber()).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void cleanup(boolean isLastPart) {

    }
}
