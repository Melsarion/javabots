package MBot;
import Communication.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class MbotWirelessCommunication extends ReadFromDeviceInterface implements MbotBasicCommunicator {
    private final int VENDOR_ID = 0x416;
    private final int PRODUCT_ID = 0xffffffff;
    private final String SERIAL_NUMBER = null;
    private final int LENGTH_OF_PREFIX = 2;
    //device constants
    private final byte ULTRASONIC_SENSOR = 1;
    private final byte TEMPERATURE_SENSOR = 2;
    private final byte LIGHT_SENSOR = 3;
    private final byte RGBLED = 8;
    private final byte MOTOR = 10;
    private final byte SERVO = 11;
    private final byte INFRARED = 16;
    private final byte LINEFOLLOWER = 17;
    private final byte TONE = 34;
    private final byte BUTTON_INNER = 35;
    private final byte DATATYPE_BYTE = 1;
    private final byte DATATYPE_FLOAT = 2;
    private final byte DATATYPE_SHORT = 3;
    private final byte DATATYPE_STRING = 4;
    private final byte DATATYPE_DOUBLE = 5;
    private AsyncReadAndWriteCommunication readFromDeviceCommunicator = new AsyncReadAndWriteCommunication();
    private byte currentReplyIndex = 0x00;
    private HIDCommunication communicator;
    private MbotDeviceInterface device;
    private byte[] previousUnparsedData = new byte[0];

    MbotWirelessCommunication() {
        communicator = new HIDCommunication(VENDOR_ID, PRODUCT_ID, SERIAL_NUMBER);
        device = null;
    }

    MbotWirelessCommunication(MbotDevice device) {
        this();
        this.device = device;
    }

    public void start() {
        communicator.startHIDService();
        readFromDeviceCommunicator.startContinuousRead(this);
    }

    @Override
    public void stop() {
        communicator.stopHIDService();
    }

    private byte generateUniqueIndex() {
        currentReplyIndex++;
        //0x80 = reply of button
        if (currentReplyIndex == (byte) 0x00 || currentReplyIndex == (byte) 0x80) {
            currentReplyIndex++;
        }
        return currentReplyIndex;
    }

    private void sendCommand(byte[] command) {
        sendMessageToDevice(command, command.length);
    }

    private CompletableFuture<byte[]> sendReadCommand(byte[] command, byte index) {
        //note: first byte is the data type, start from second byte is the data
        return readFromDeviceCommunicator.readMessageFromDevice(command, command.length, String.valueOf(index));
    }

    private byte[] addPrefix(byte[] command) {
        byte[] messageWithPrefix = new byte[command.length + LENGTH_OF_PREFIX];
        messageWithPrefix[0] = (byte) 0xFF;
        messageWithPrefix[1] = (byte) 0x55;
        System.arraycopy(command, 0, messageWithPrefix, 2, command.length);
        return messageWithPrefix;
    }

    private byte[] addWriteHead(byte[] message) {
        return addHead(message, (byte) 0x02, (byte) 0x00);
    }

    private byte[] addReadHead(byte[] message, byte index) {
        return addHead(message, (byte) 0x01, index);
    }

    private byte[] addHead(byte[] command, byte readOrWrite, byte answerIndex) {
        //add length, index and Read/Write
        byte[] messageWithHead = new byte[command.length + 3];
        messageWithHead[0] = (byte) (command.length + 2); // message + byte index + byte Read/Write
        messageWithHead[1] = answerIndex;
        messageWithHead[2] = readOrWrite;
        System.arraycopy(command, 0, messageWithHead, 3, command.length);
        return addPrefix(messageWithHead);
    }

    //speed between -255 and 255, only motor 1 and 2 possible
    public void setDCMotor(int speed, Motor motorSide) {
        //port is 9 or a, speed_low = speed & 0xff, speed_high = (speed>>8) 0xff (0xff to go from signed int to unsigned byte)
        //todo add check for entered variables
        byte[] command = new byte[4];
        //the left motor is assembled in mirror, so travelling forward is by turning the motor in the opposite direction
        int speedForward = motorSide == Motor.RIGHT? speed: -speed;
        command[0] = MOTOR;
        command[1] = motorSide == Motor.LEFT ? (byte) 0x09 : (byte) 0x0a;
        command[2] = (byte) (speedForward & 0xff); //speed_low
        command[3] = (byte) ((speedForward >> 8) & 0xff); //speed_high
        command = addWriteHead(command);
        sendCommand(command);
    }

    public void setBuzzerTone(int toneInHz, int time) {
        byte[] command = new byte[5];
        command[0] = TONE;
        command[1] = (byte) ((short)toneInHz & 0xff); //tone_low
        command[2] = (byte) (((short)toneInHz >> 8) & 0xff); //tone_high
        command[3] = (byte) ((short)time & 0xff); // beat_low
        command[4] = (byte) (((short)time >> 8) & 0xff); // beat_high
        command = addWriteHead(command);
        sendCommand(command);
    }
    public void setBuzzerTone(int toneInHz){
        setBuzzerTone(toneInHz,0x0a); //default value as said in the protocol documentation
    }

    //RGB 0-255
    public void setLedColor(boolean leftLed, boolean rightLed, int R, int G, int B) {
        if (!leftLed && !rightLed) return; //there is no led to change
        byte[] command = new byte[7];
        command[0] = RGBLED;
        command[1] = 0x07; //port
        command[2] = 0x02; //slot
        command[3] = leftLed ? (rightLed ? (byte) 0x00 : (byte) 0x01) : ((byte) 0x02);
        command[4] = (byte) R;
        command[5] = (byte) G;
        command[6] = (byte) B;
        command = addWriteHead(command);
        sendCommand(command);
    }

    private int convertByteArrayToInteger(byte[] byteArray) {
        return (byteArray[0] << 24) + (byteArray[1] << 16) + (byteArray[2] << 8) + byteArray[3];
    }

    CompletableFuture<Float> readUltrasonicSensorAsync(int portNumber) {
        if (portNumber > 4) {
            System.out.println("error: invalid port number assigned to read Ultrasonic Sensor: port number " + portNumber);
            return CompletableFuture.failedFuture(new IllegalArgumentException());
        }
        byte[] command = new byte[2];
        command[0] = ULTRASONIC_SENSOR;
        command[1] = (byte) portNumber; //port
        byte index = generateUniqueIndex();
        command = addReadHead(command, index);
        return sendReadCommand(command, index).thenApply(this::decapsulateAsFloat);
    }

    public float readUltrasonicSensor(int portNumber){
        try {
            return readUltrasonicSensorAsync(portNumber).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            System.exit(-1);
            return 0;
        }
    }

    CompletableFuture<LineFollowerState> readLineFollowSensorAsync(int portNumber) {
        if (portNumber > 4) {
            throw new IllegalArgumentException("Invalid port number assigned to read Line Follow Sensor: port number " + portNumber + ". Port number should be 1,2,3 or 4.");
        }
        byte[] command = new byte[2];
        command[0] = LINEFOLLOWER;
        command[1] = (byte) portNumber; //port
        byte index = generateUniqueIndex();
        command = addReadHead(command, index);
        return sendReadCommand(command, index).thenApply(this::decapsulateLineFollowState);
    }
    public LineFollowerState readLineFollowSensor(int portNumber){
        try {
            return readLineFollowSensorAsync(portNumber).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    private LineFollowerState decapsulateLineFollowState(byte[] reply) {
        //hardcoded as the returned bytes have no symantic sense
        boolean left = reply[4] == 0x40;
        boolean right = ((reply[3] == (byte) 0x80 && reply[4] == (byte) 0x3F) || (reply[3] == 0x40));
        if(left){
            if(right){
                return LineFollowerState.BOTH;
            }else{
                return LineFollowerState.LEFT;
            }
        }else{
            if(right){
                return LineFollowerState.RIGHT;
            }else{
                return LineFollowerState.NONE;
            }
        }
    }

    private void onButtonPress() {
        if (device != null) {
            device.onDeviceButtonPress();
        }
    }
    private void onButtonRelease(){
        if (device != null) {
            device.onDeviceButtonRelease();
        }
    }

    private Float decapsulateAsFloat(byte[] reply) {
        //check if it is indeed an int
        if (reply[0] != DATATYPE_FLOAT || reply.length != 5) {
            System.out.println("error in decapsulating the float");
        }
        return Float.intBitsToFloat(convertByteArrayToInteger(reply));
    }

    protected void sendMessageToDevice(byte[] readCommand, int length) {
        communicator.sendMessageToDevice(readCommand, length);
    }

    protected byte[] readMessage() {
        return communicator.readMessageFromDevice();
    }

    protected void parseData(byte[] data) {
        //frame is surrounded by 0xff 0x55 and 0x0d 0x0a
        //System.out.println("PARSE DATA " + HIDCommunication.ByteArrayToString(data));
        //add unchecked part of previous frame to current frame at start
        byte[] allFrames = new byte[data.length + previousUnparsedData.length];
        System.arraycopy(previousUnparsedData, 0, allFrames, 0, previousUnparsedData.length);
        System.arraycopy(data, 0, allFrames, previousUnparsedData.length, data.length);

        //check for needed start of frame
        int index = 0;
        int startCurrentFrame = -1;
        while (index < allFrames.length - 1) {
            startCurrentFrame = -1;
            //start of frame
            if (allFrames[index] == (byte) 0xff && allFrames[index + 1] == (byte) 0x55) {
                startCurrentFrame = index;
                while (index < allFrames.length - 1) {
                    //find end suffix
                    if (allFrames[index] == (byte) 0x0d && allFrames[index + 1] == (byte) 0x0a) {
                        //frame found, reply index is always the third byte
                        //System.out.println(Util.ByteArrayToString(allFrames));

                        if (startCurrentFrame + 2 == index) {
                            //empty frame
                        }
                        //check if it is a button reply
                        //Button is 0x80, OxO1 (defining a byte) and byte value, being 1 for press, 0 for release of button
                        else if (allFrames[startCurrentFrame + 2] == (byte) 0x80 && (allFrames[startCurrentFrame + 3] == 0x01) && (allFrames[startCurrentFrame + 4] == 0x01||allFrames[startCurrentFrame + 4] == 0x00)) {
                            byte buttonValue = allFrames[startCurrentFrame + 4];
                            if(buttonValue == 0x00) {
                                onButtonRelease();
                            }else{
                                onButtonPress();
                            }
                        } else {
                            byte replyIndexOfFrame = allFrames[startCurrentFrame + 2];

                            byte[] dataOfFrame = new byte[index - startCurrentFrame - 3];
                            System.arraycopy(allFrames, startCurrentFrame + 3, dataOfFrame, 0, dataOfFrame.length);
                            //System.out.print("Frame found with with bytes: " + Util.ByteArrayToString(dataOfFrame) + "\n");
                            readFromDeviceCommunicator.messageReply(String.valueOf(replyIndexOfFrame), dataOfFrame);
                        }
                        startCurrentFrame = -1;
                        //start search for next frame
                        break;
                    }
                    index++;
                }

            }
            index++;
        }

        if (startCurrentFrame != -1) {
            //data left to be concatenated with next frame
            previousUnparsedData = new byte[allFrames.length - startCurrentFrame];
            System.arraycopy(allFrames, startCurrentFrame, previousUnparsedData, 0, previousUnparsedData.length);
        } else {
            previousUnparsedData = new byte[0];
        }
    }


}

