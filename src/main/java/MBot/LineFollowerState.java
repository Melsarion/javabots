package MBot;

/**
 * An enum that represents the state of the line follower sensor that can be connected to an mbot.
 * It has two sensors, a left sensor and right sensor, that can be either active or inactive. The name denotes which sensor is on.
 * For example LEFT means the left sensor is active and the right sensor is inactive. Active means there is no light detected (black underground).
 */
public enum LineFollowerState {
    LEFT(true,false),
    RIGHT(false,true),
    BOTH(true,true),
    NONE(false,false);

    private boolean leftSensor;
    private boolean righSensor;

    LineFollowerState(boolean leftSensor, boolean righSensor) {
        this.leftSensor = leftSensor;
        this.righSensor = righSensor;
    }

    /**
     * check the left sensor.
     * @return true if left sensor is active.
     */
    public boolean lefSensor(){
        return leftSensor;
    }
    /**
     * check the right sensor.
     * @return true if right sensor is active.
     */
    public boolean rightSensor(){
        return righSensor;
    }

    /**
     * check if both the left and right sensor are active.
     * @return true if both sensors are active.
     */
    public boolean bothSensors(){
        return leftSensor && righSensor;
    }
    /**
     * check if neither left nor right sensor is active.
     * @return true if both sensors are inactive.
     */
    public boolean noneSensors(){
        return !leftSensor && !righSensor;
    }

}
