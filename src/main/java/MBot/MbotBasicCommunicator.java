package MBot;

import Communication.Communicator;

public interface MbotBasicCommunicator extends Communicator {
    void setDCMotor(int speed, Motor motorSide);
    void setBuzzerTone(int toneInHz, int time);
    void setLedColor(boolean leftLed, boolean rightLed, int R, int G, int B);
    float readUltrasonicSensor(int portNumber);
    LineFollowerState readLineFollowSensor(int portNumber);




}
