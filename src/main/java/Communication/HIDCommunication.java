package Communication;

import org.hid4java.*;
import org.hid4java.event.HidServicesEvent;
import org.hid4java.jna.HidApi;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;


public class HIDCommunication implements HidServicesListener {

    //move to global variable given in constructor
    private Integer vendorID; // 0x416 for mbot
    private Integer productID; // 0xffffffff for mbot
    private String serialNumber; //null for wildcard
    private static final int PACKET_LENGTH = 64;
    private HidDevice connectedHidDevice;
    private HidServices hidServices;
    private static final int maxRetriesConnectService = 5;

    public HIDCommunication(Integer vendorID, Integer productID, String serialNumber) {
        this.vendorID = vendorID;
        this.productID = productID;
        this.serialNumber = serialNumber;
    }
    public HIDCommunication(Integer vendorID, Integer productID) {
        this(vendorID, productID, null);
    }

    public void startHIDService() {
        hidServices = HidManager.getHidServices();
        startHIDService(hidServices);
    }

    public void startHIDService(HidServices hidServices) {
        this.hidServices = hidServices;
        int communicationTries = 0;
        //startup should only be done once
        // Start the services
        System.out.println("Starting HID services.");
        hidServices.start();

        //System.out.println("Enumerating attached devices...");

        // Provide a list of attached devices
        //for (HidDevice hidDevice : hidServices.getAttachedHidDevices()) {
        //    System.out.println(hidDevice);
        //}

        // Open the device device by Vendor ID and Product ID with serial number (null = serial number is wildcard)
        while(communicationTries < maxRetriesConnectService) {
            HidDevice hidDevice = hidServices.getHidDevice(vendorID, productID, serialNumber);
            if (hidDevice != null) {
                System.out.println("Hid devices found");
                //startup successful
                HidApi.dropReportIdZero = true;
                connectedHidDevice = hidDevice;
            } else {
                System.out.println("no HID device found. Make sure the device is connected. Retrying in one second.");
                communicationTries++;
            }
            try {
                //could also wait for an update in hidDeviceAttached
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopHIDService() {
        hidServices.shutdown();
    }

    public void sendMessageToDevice(byte[] message, int length) {
        // Ensure device is open after an attach/detach event
        if (!connectedHidDevice.isOpen()) {
            connectedHidDevice.open();
        }
        //System.out.println("send a message via HID");
        byte[] messagePacket = new byte[PACKET_LENGTH];

        //head
        messagePacket[0] = (byte) (0x00); //ignored bytes (HID identifier wildcard)
        messagePacket[1] = (byte) (length); //length of message
        //append message to head
        if (length >= 0) System.arraycopy(message, 0, messagePacket, 2, length);
        //System.out.println("with bytes: " + Util.ByteArrayToString(messagePacket));
        connectedHidDevice.write(messagePacket, PACKET_LENGTH, (byte) 0x00);
    }

    public void readMessageFromDevice(byte[] dataBuffer) {
        connectedHidDevice.read(dataBuffer, 1000);
    }
    public byte[] readMessageFromDevice(){
        byte[] data = new byte[PACKET_LENGTH];
        Arrays.fill(data,(byte)0x00);
        readMessageFromDevice(data);
        return data;
    }

    @Override
    public void hidDeviceAttached(HidServicesEvent hidServicesEvent) {
        System.out.println("Device attached: " + hidServicesEvent);
        // Add serial number when more than one device with the same
        // vendor ID and product ID will be present at the same time
        /*if (hidServicesEvent.getHidDevice().isVidPidSerial(VENDOR_ID, PRODUCT_ID, null)) {
            sendMessage(hidServicesEvent.getHidDevice());
        }*/
    }

    @Override
    public void hidDeviceDetached(HidServicesEvent hidServicesEvent) {
        System.err.println("Device detached: " + hidServicesEvent);
    }

    @Override
    public void hidFailure(HidServicesEvent hidServicesEvent) {
        System.err.println("HID failure: " + hidServicesEvent);
    }
}