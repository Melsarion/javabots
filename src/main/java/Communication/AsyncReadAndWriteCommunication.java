package Communication;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AsyncReadAndWriteCommunication {
    private Thread readingThread;
    private Map<String, DeviceCommunicatedData> indexReadMap = new HashMap<>();
    private final ExecutorService readDeviceExecutor = Executors.newCachedThreadPool();
    private ReadFromDeviceInterface readFromDeviceInterface;
    private boolean readOutput = false;

    public void startContinuousRead(ReadFromDeviceInterface readFromDeviceInterface){
        this.readFromDeviceInterface = readFromDeviceInterface;

        if(!readOutput){
            readOutput = true;
            readingThread = new Thread(() -> {
                byte[] data;
                while (readOutput) {
                    //read answer
                    data = readFromDeviceInterface.readMessage();
                    readFromDeviceInterface.parseData(data);
                }
            });
            readingThread.start();
        }
    }

    public CompletableFuture<byte[]> readMessageFromDevice(byte[] readCommand, int length, String messageId) {
        if (!readOutput) {
            System.out.println("error: can't read message from device. First call startContinuousRead in AsynReadAndWriteCommunication to start the service.");
            return null;
        }

        DeviceCommunicatedData deviceData = new DeviceCommunicatedData();
        indexReadMap.put(messageId, deviceData);

        return CompletableFuture.supplyAsync(() -> {
            readFromDeviceInterface.sendMessageToDevice(readCommand, length);
            //wait for a notify that is called when idData is updated
            synchronized (deviceData) {
                try {
                    deviceData.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return deviceData.getData();
        },readDeviceExecutor);
    }

    public void stopContinuousRead(){
        readOutput = false;
        try {
            readingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        readingThread = null;
    }



    public void messageReply(String messageId, byte[] message){
        if (indexReadMap.containsKey(messageId)) {
            synchronized (indexReadMap.get(messageId)) {
                indexReadMap.get(messageId).setData(message);
                indexReadMap.get(messageId).notify();
            }
            indexReadMap.remove(messageId);
        }
    }
}


