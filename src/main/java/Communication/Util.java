package Communication;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.vecmath.Vector3d;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

public class Util {
    public static String[] jsonArraytoStringArray(JsonArray array) {
        if (array == null) {
            return null;
        }
        String[] arr = new String[array.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = array.getString(i);
        }
        //System.out.println(Arrays.toString(arr));
        return arr;
    }

    public static String BytesToStringBase64(byte[] bytes){
        byte[] encoded = Base64.getEncoder().encode(bytes);
        return new String(encoded);
    }
    public static byte[] stringToBytesBase64(String data){
        return Base64.getDecoder().decode(data);
    }
    public static int setBitOfInt(int state, int bits, int bitNumber){
        if(state == 0){
            return clearBitOfInt(bits,bitNumber);
        }else if(state == 1){
            return setBitOfInt(bits,bitNumber);
        }else{
            throw new IllegalArgumentException("error in setBitOfInt. State was not 0 or 1.");
        }
    }
    public static int setBitOfInt(int bits, int bitNumber){
        bits |= 1 << bitNumber;
        return bits;
    }

    public static int clearBitOfInt(int bits, int bitNumber){
        bits &= ~(1 << bitNumber);
        return bits;
    }
    public static boolean readBitOfInt(int bits, int bitNumber){
        return ((bits & 1 << bitNumber) != 0);
    }


    public static JsonObject jsonFromString(String jsonObjectStr) {
        JsonReader jsonReader = Json.createReader(new StringReader(jsonObjectStr));
        JsonObject object = jsonReader.readObject();
        jsonReader.close();

        return object;
    }

    //print bytes from StackOverflow
    //move to more general class?
    public static String ByteArrayToString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        for (byte b : bytes) {
            sb.append(String.format("0x%02X ", b));
        }
        sb.append("]");
        return sb.toString();
    }

    //all in little endian
    public static byte[] bytesLittleEndianFromShort(short number) {
        byte[] bytes = new byte[2];
        bytes[0] = (byte) (number & 0xff);
        bytes[1] = (byte) ((number >> 8) & 0xff);
        return bytes;
    }

    private static byte[] bytesLittleEndianFromTwoShorts(short number1, short number2) {
        byte[] bytes = new byte[4];
        bytes[0] = (byte) (number1 & 0xff);
        bytes[1] = (byte) ((number1 >> 8) & 0xff);
        bytes[2] = (byte) (number2 & 0xff);
        bytes[3] = (byte) ((number2 >> 8) & 0xff);
        return bytes;
    }

    public static byte[] bytesLittleEndianFromInt(int s) {
        byte[] bytes = new byte[4];
        bytes[0] = (byte) (s & 0xff);
        bytes[1] = (byte) ((s >> 8) & 0xff);
        bytes[2] = (byte) ((s >> 16) & 0xff);
        bytes[3] = (byte) ((s >> 24) & 0xff);
        return bytes;
    }

    public static Vector3d vector3dFromShortLittleEndianBytes(byte[] bytes) {
        if (bytes.length != 6) {
            throw new IllegalArgumentException("wrong amount of bytes for Coordinates.");
        }
        Vector3d coords = new Vector3d();
        coords.setX(shortFromLittleEndianBytes(bytes[0], bytes[1]));
        coords.setY(shortFromLittleEndianBytes(bytes[2], bytes[3]));
        coords.setZ(shortFromLittleEndianBytes(bytes[4], bytes[5]));
        return coords;
    }

    public static short shortFromLittleEndianBytes(byte[] bytes) {

        if (bytes == null || bytes.length != 2) {
            return 0;
        }

        short result = 0;
        result = (short) (((bytes[1] & 0xff) << 8) + (bytes[0] & 0xff));
        if ((result | 0x8000) == 0x8000) {
            result = (short) (result * -1);
        }
        return result;
    }

    private static short shortFromLittleEndianBytes(byte b0, byte b1) {

        short result = 0;
        result = (short) (((b1 & 0xff) << 8) + (b0 & 0xff));
        if ((result | 0x8000) == 0x8000) {
            result = (short) (result * -1);
        }
        return result;
    }

    public static int intFromLittleEndianBytes(byte[] bytes) {

        if (bytes == null || bytes.length != 4) {
            return 0;
        }

        int result = 0;
        result = ((bytes[3] << 24) + (bytes[2] << 16) + (bytes[1] << 8) + bytes[0]);
        if ((result | 0x80000000) == 0x80000000) {
            result = (result * -1);
        }
        return result;
    }

    /*
        creates a usable UUID string
         */
    public static String normalizeUUID(String UUID) {
        return (UUID.substring(0, 8) + "-" + UUID.substring(8, 12) + "-" + UUID.substring(12, 16)
                + "-" + UUID.substring(16, 20) + "-" + UUID.substring(20))
                .toLowerCase();
    }

    public static byte[] textToBytesUTF8(String text){
        return text.getBytes(StandardCharsets.UTF_8);
    }

    public static Vector3d lowPassFilter( Vector3d vectorNewValue, Vector3d vectorPreviousValue) {
        float ALPHA = 0.45f;
        if ( vectorPreviousValue == null ) return vectorNewValue;
        Vector3d newValue = new Vector3d();
        newValue.x = vectorPreviousValue.x + ALPHA * (vectorNewValue.x - vectorPreviousValue.x);
        newValue.y = vectorPreviousValue.y + ALPHA * (vectorNewValue.y - vectorPreviousValue.y);
        newValue.z = vectorPreviousValue.z + ALPHA * (vectorNewValue.z - vectorPreviousValue.z);
        return newValue;
    }
}
