package Communication;

public class DeviceCommunicatedDataBLE extends DeviceCommunicatedData{
    private String encoding;
    private String message;
    private String[] messageArray;
    private String fullJsonMessage;
    private String serviceId;
    private String characteristicId;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public byte[] getData() {
        return getByteMessage();
    }

    @Override
    public void setData(byte[] data) {
        super.setData(data);
    }
    public void setByteMessage(byte [] data){
        setData(data);
    }

    //mainly used for debugging purposes
    public String getFullJsonMessage() {
        return fullJsonMessage;
    }

    public void setFullJsonMessage(String fullJsonMessage) {
        this.fullJsonMessage = fullJsonMessage;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public boolean isByteMessage() {
        return  super.getData() != null || (encoding != null && encoding.equals("base64"));
    }

    public byte[] getByteMessage() {
        if (super.getData() != null ){
            return super.getData();
        }
        else if (isByteMessage()) {
            return Util.stringToBytesBase64(this.message);
        } else if(!message.equals("")) {
            System.out.println("error: not a byte message");
            return new byte[0];
        }
            else if(messageArray!=null){
            System.out.println("error: no message present. Try getMessageArray(), as there is array data present.");
            return new byte[0];
            }
         else{
            System.out.println("error: no data present");
            return new byte[0];
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getMessageArray() {
        return messageArray;
    }

    public void setMessageArray(String[] messageArray) {
        this.messageArray = messageArray;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getCharacteristicId() {
        return characteristicId;
    }

    public void setCharacteristicId(String characteristicId) {
        this.characteristicId = characteristicId;
    }
}
