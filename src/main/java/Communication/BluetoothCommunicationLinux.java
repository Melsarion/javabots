package Communication;

import tinyb.BluetoothDevice;
import tinyb.BluetoothGattCharacteristic;
import tinyb.BluetoothGattService;
import tinyb.BluetoothManager;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class BluetoothCommunicationLinux implements BluetoothCommunicationInterface {

    //reading via bluetooth are many small tasks with unknown amount
    //often only one read will be done at a time, but support for multiple reads is needed for specific cases
    private final ExecutorService readDeviceExecutor = Executors.newCachedThreadPool();
    private static final BluetoothManager manager = BluetoothManager.getBluetoothManager();
    private BluetoothDevice device;
    private Map<String, BluetoothGattService> servicesMap = new HashMap<>();
    private Map<String, BluetoothGattCharacteristic> characteristicMap = new HashMap<>();
    private final CountDownLatch stayActiveLock = new CountDownLatch(1);
    private Thread idleThread;

    private BluetoothParseInterface dataParser;

    public BluetoothCommunicationLinux(BluetoothParseInterface dataParser) {
        this.dataParser = dataParser;
    }

    //all peripheral notifications will be ignored
    BluetoothCommunicationLinux() {
        this.dataParser = idData -> {
            //do not parse messages
        };
    }

    private void onMessage(String serviceId, String characteristicId, byte[] message) {
        //System.out.println(serviceId + "  " +  Util.ByteArrayToString(message));
        DeviceCommunicatedDataBLE idData = new DeviceCommunicatedDataBLE();
        idData.setServiceId(serviceId);
        idData.setCharacteristicId(characteristicId);
        idData.setByteMessage(message);
        dataParser.parse(idData);
    }

    public void start() {
        if(idleThread == null) {
            idleThread = new Thread(() -> {
                try {
                    stayActiveLock.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        idleThread.start();
    }
    public void stop(){
        stayActiveLock.countDown();
        idleThread = null;
    }

    public void connectToDeviceAllServices(String name) {
        connectToDevice(name);
    }

    public void connectToDevice(String name, String[] optionalServices) throws InterruptedException {
        boolean discoveryStarted = manager.startDiscovery();
        System.out.println("The discovery started: " + (discoveryStarted ? "true" : "false"));

        device = getDevice(name);
        //device found, stop discovery
        manager.stopDiscovery();
        System.out.println("Device found: ");
        if (device == null) {
            System.out.println("no device found");
            return;
        } else {
            printDevice(device);
        }

        //connect to the device
        boolean connected = device.connect();
        if (!connected) {
            System.out.println("could not connect");
        }
        //save services and characteristics in Hash Map for easy use
        //services not in optional services will just not be added to the map here. However if it is null, all services are added
        ArrayList<String> optionalServicesAL = null;
        if (optionalServices != null) {
            optionalServicesAL = new ArrayList<>(Arrays.asList(optionalServices));
        }

        if(!device.getServicesResolved()){
            Thread.sleep(100);
        }
        if(!device.getServicesResolved()){
            System.out.println("services still not resolved after waiting 100ms.");
        }
        List<BluetoothGattService> services = device.getServices();
        for (BluetoothGattService service : services) {
            if (optionalServicesAL == null || optionalServicesAL.contains(service.getUUID()))
                servicesMap.put(service.getUUID(), service);
            for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                characteristicMap.put(characteristic.getUUID(), characteristic);
            }
        }
    }

    private static BluetoothDevice getDevice(String address) throws InterruptedException {
        BluetoothDevice sensor = null;
        for (int i = 0; (i < 15); ++i) {
            List<BluetoothDevice> list = manager.getDevices();
            if (list == null)
                return null;

            for (BluetoothDevice device : list) {
                //printDevice(device);
                /*
                 * Here we check if the address matches.
                 */
                if (device.getName().equals(address))
                    sensor = device;
            }

            if (sensor != null) {
                return sensor;
            }
            Thread.sleep(500);
        }
        return null;
    }

    private static void printDevice(BluetoothDevice device) {
        System.out.print("Address = " + device.getAddress());
        System.out.print(" Name = " + device.getName());
        System.out.print(" Connected = " + device.getConnected());
        System.out.println();
    }

    public void connectToDevice(String name) {
        try {
            connectToDevice(name, null);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Future<DeviceCommunicatedDataBLE> sendReadMessage(String serviceId, String characteristicId) {
        BluetoothGattCharacteristic charac = characteristicMap.get(characteristicId);
        DeviceCommunicatedDataBLE idData = new DeviceCommunicatedDataBLE();
        idData.setServiceId(serviceId);
        idData.setCharacteristicId(characteristicId);
        return readDeviceExecutor.submit(() -> {
                    byte[] valueRaw = charac.readValue();
                    idData.setByteMessage(valueRaw);
                    return idData;
                }
        );
    }

    public String[] getServices() {
        return servicesMap.keySet().toArray(new String[0]);
    }

    public void sendWriteCommand(String serviceId, String characteristicId, byte[] data) {
        characteristicMap.get(characteristicId).writeValue(data);
    }

    public void sendStartNotificationsCommand(String serviceId, String characteristicId) {
        characteristicMap.get(characteristicId).enableValueNotifications(bytes -> readDeviceExecutor.execute(() -> onMessage(serviceId, characteristicId, bytes)));
    }

    public void sendStopNotificationsCommand(String serviceId, String characteristicId) {
        characteristicMap.get(characteristicId).disableValueNotifications();
    }

    public void sendNotificationsStateCommand(String serviceId, String characteristicId, boolean state) {
        if (state) {
            sendStartNotificationsCommand(serviceId, characteristicId);
        } else {
            sendStopNotificationsCommand(serviceId, characteristicId);
        }
    }

    public BluetoothCommunicationInterface getInstance() {
        return this;
    }
}
