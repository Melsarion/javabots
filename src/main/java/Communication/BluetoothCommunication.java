package Communication;

import org.apache.commons.lang3.SystemUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Future;

public class BluetoothCommunication implements BluetoothCommunicationInterface {
    private BluetoothCommunicationInterface communicator;

    public BluetoothCommunication(BluetoothParseInterface dataParser) {
        if (SystemUtils.IS_OS_LINUX) {
            communicator = new BluetoothCommunicationLinux(dataParser);
        } else {
            try {
                communicator = new BluetoothCommunicationScratchLink(dataParser);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    //all peripheral notifications will be ignored
    public BluetoothCommunication() {
        if (SystemUtils.IS_OS_LINUX) {
            communicator = new BluetoothCommunicationLinux();
        } else {
            try {
                communicator = new BluetoothCommunicationScratchLink();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public void start() {
        communicator.start();
    }
    public void stop(){ communicator.stop();}

    public void connectToDeviceAllServices(String name) {
        communicator.connectToDevice(name);
    }

    public void connectToDevice(String name) {
        connectToDeviceAllServices(name);
    }


    public Future<DeviceCommunicatedDataBLE> sendReadMessage(String serviceId, String characteristicId) {
        return communicator.sendReadMessage(serviceId, characteristicId);
    }

    public String[] getServices() {
        return communicator.getServices();
    }


    public void sendWriteCommand(String serviceId, String characteristicId, byte[] data) {
        communicator.sendWriteCommand(serviceId, characteristicId, data);
    }

    public void sendStartNotificationsCommand(String serviceId, String characteristicId) {
        communicator.sendStartNotificationsCommand(serviceId, characteristicId);
    }

    public void sendStopNotificationsCommand(String serviceId, String characteristicId) {
        communicator.sendStopNotificationsCommand(serviceId, characteristicId);
    }

    public void sendNotificationsStateCommand(String serviceId, String characteristicId, boolean state) {
        if (state) {
            sendStartNotificationsCommand(serviceId, characteristicId);
        } else {
            sendStopNotificationsCommand(serviceId, characteristicId);
        }
    }

    public BluetoothCommunicationInterface getInstance(){
        return communicator;
    }
}
