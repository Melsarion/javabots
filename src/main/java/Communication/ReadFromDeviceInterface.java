package Communication;

public abstract class ReadFromDeviceInterface {
    //how to send a message to the device via the used transport method
    abstract protected void sendMessageToDevice(byte[] readCommand, int length);

    //general read from the transport of a device message
    abstract protected byte[] readMessage();

    //parse string of bytes to get out the data. call messageReply for a reply. Note that it will return an array with potentially 0x00 trailing bytes
    abstract protected void parseData(byte[] data);
}
