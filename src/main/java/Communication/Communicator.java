package Communication;

public interface Communicator {
    void start();
    void stop();
}
