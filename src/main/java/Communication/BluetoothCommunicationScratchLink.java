package Communication;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import javax.json.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.*;

public class BluetoothCommunicationScratchLink extends WebSocketClient implements BluetoothCommunicationInterface {
    private final CountDownLatch connectionLock = new CountDownLatch(1);

    //reading via bluetooth are many small tasks with unknown amount
    //often only one read will be done at a time, but support for multiple reads is needed for specific cases
    private final ExecutorService readDeviceExecutor = Executors.newCachedThreadPool();
    private int counter = 0;
    private String lastFoundPeripheralId = "";
    /* the same key is never accessed or changed at the same time in this implementation
    hence using a ConcurrentHashMap is enough to avoid thread problems. No synchronization blocks are needed
     */
    private final Map<String, DeviceCommunicatedDataBLE> CommunicationDataMap = new ConcurrentHashMap<>();
    private BluetoothParseInterface dataParser;

    public BluetoothCommunicationScratchLink(BluetoothParseInterface dataParser) throws URISyntaxException {
        super(new URI("wss://0.0.0.0:20110/scratch/ble"));
        this.dataParser = dataParser;
    }

    //all peripheral notifications will be ignored
    BluetoothCommunicationScratchLink() throws URISyntaxException {
        super(new URI("wss://0.0.0.0:20110/scratch/ble"));
        this.dataParser = idData -> {
            //do not parse messages
        };
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        System.out.println("opened connection with Scratch Link");
    }

    @Override
    public void onMessage(String message) {
        //System.out.println("received: " + message);
        JsonObject jsonMessage = Util.jsonFromString(message);

        if (jsonMessage.containsKey("method") && jsonMessage.getString("method").equals("didDiscoverPeripheral")) {
            lastFoundPeripheralId = "" + Util.jsonFromString(message).getJsonObject("params").getJsonNumber("peripheralId");
            connectionLock.countDown();
        } else if (jsonMessage.containsKey("id") && CommunicationDataMap.containsKey(jsonMessage.getString("id"))) {
            String id = jsonMessage.getString("id");
            if (jsonMessage.containsKey("result")) {
                Object result = jsonMessage.isNull("result")? null: jsonMessage.get("result");
                //reply
                if (result instanceof JsonArray) {
                    synchronized (CommunicationDataMap.get(id)) {
                        CommunicationDataMap.get(id).setMessageArray(Util.jsonArraytoStringArray(jsonMessage.getJsonArray("result")));
                        CommunicationDataMap.get(id).setFullJsonMessage(message);
                        CommunicationDataMap.get(id).notify();
                        CommunicationDataMap.remove(id);
                    }
                } else {
                    synchronized (CommunicationDataMap.get(id)) {
                        if(result != null) {
                            if (((JsonObject) result).containsKey("encoding")) {
                                CommunicationDataMap.get(id).setEncoding(((JsonObject) result).getString("encoding"));
                            }
                            CommunicationDataMap.get(id).setMessage(((JsonObject) result).getString("message"));
                        }else{
                            CommunicationDataMap.get(id).setMessage(null);
                        }
                        CommunicationDataMap.get(id).setFullJsonMessage(message);
                        CommunicationDataMap.get(id).notify();
                        CommunicationDataMap.remove(id);
                    }
                }
            }
            else if (jsonMessage.containsKey("error")) {
                //when receiving an error, the object will still be send but with an empty message
                //and error filled in. This way a program will not hold on waiting for a message.
                //It is up to other classes to decide to check for these errors or deal with just an empty message value
                synchronized (CommunicationDataMap.get(id)) {
                    CommunicationDataMap.get(id).setErrorMessage(jsonMessage.getJsonObject("error").getString("message"));
                    CommunicationDataMap.get(id).setMessage("");
                    CommunicationDataMap.get(id).notify();
                    CommunicationDataMap.remove(id);
                }
                String errorCode = jsonMessage.getJsonObject("error").getString("code");
                switch (errorCode){
                    case "-32700": //Parse Error

                    case "-32600": //"Invalid Request"

                    case "-32601": //Method not found

                    case "-32602": //Invalid params

                    case "-32603": //Internal error

                    case "-32500": //Application error
                        System.out.println("Scratch Link error: Application error. Scratch link might not be in the correct state to process this call.");
                    default:
                        System.out.println("Scratch Link error: Server error ");
                }
            }
        }//non-reply message
        else if (jsonMessage.containsKey("method") && jsonMessage.getString("method").equals("characteristicDidChange")) {
            DeviceCommunicatedDataBLE idData = new DeviceCommunicatedDataBLE();
            idData.setFullJsonMessage(message);
            JsonObject params = jsonMessage.getJsonObject("params");
            idData.setServiceId(params.getString("serviceId"));
            idData.setCharacteristicId(params.getString("characteristicId"));
            idData.setMessage(params.getString("message"));
            idData.setEncoding(params.getString("encoding"));
            dataParser.parse(idData);
        }
        else{
            //System.out.println("error: unhandled messages in BluetoothCommunication: " + message);
        }
    }


    @Override
    public void onClose(int code, String reason, boolean remote) {
        System.out.println("Connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: " + reason);
    }

    @Override
    public void onError(Exception ex) {
        ex.printStackTrace();
        // if the error is fatal then onClose will be called additionally
    }

    private JsonObjectBuilder jsonRPCBase(String method, int id) {
        return jsonRPCBase(method, "" + id);
    }

    private JsonObjectBuilder jsonRPCBase(String method) {
        counter++;
        return jsonRPCBase(method, counter);
    }

    private JsonObjectBuilder jsonRPCBase(String method, String id) {
        return Json.createObjectBuilder()
                .add("jsonrpc", "2.0")
                .add("id", id)
                .add("method", method);
    }

    //include all services that you want to use in the future
    private JsonObject jsonDiscover(String deviceName, String[] requestedServices) {
        JsonObjectBuilder jsonBuilder = jsonRPCBase("discover", "discover");
        JsonObjectBuilder jsonParams = Json.createObjectBuilder();
        jsonParams.add("filters", Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("name", deviceName)));
        if (requestedServices != null) {
            JsonArrayBuilder jsonServicesBuilder = Json.createArrayBuilder();
            for (String optionalService : requestedServices) {
                jsonServicesBuilder.add(optionalService);
            }
            jsonParams.add("optionalServices", jsonServicesBuilder);
        }
        jsonBuilder.add("params", jsonParams);

        return jsonBuilder.build();
    }

    public Future<DeviceCommunicatedDataBLE> sendDiscoverCommand(String deviceName, String[] requestedServices){
        return sendReadJsonCommand(jsonDiscover(deviceName,requestedServices));
    }

    public void start() {
        try {
            boolean connected = connectBlocking();
            if(!connected){
                System.out.println("Could not connect to Scratch Link. Is Scratch Link running?");
                System.exit(-1);
            }
        } catch (InterruptedException e) {
            System.out.println("connection was interrupted");
            e.printStackTrace();
        }
    }

    public void stop(){
        shutdown();
    }

    public void connectToDeviceAllServices(String name) {
        BluetoothCommunicationScratchLink tempCommunicator;
        try {
            tempCommunicator = new BluetoothCommunicationScratchLink();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(-1);
            return;
        }
        tempCommunicator.start();
        tempCommunicator.connectToDevice(name,new String[]{});
        String[] services = tempCommunicator.getServices();
        tempCommunicator.shutdown();
        //reconnect with these services
        connectToDevice(name, services);
    }
    public void connectToDevice(String name, String[] optionalServices) {
        try {
            if (optionalServices == null) {
                connectToDeviceAllServices(name);
            } else {
                //wait for discover
                DeviceCommunicatedDataBLE reply;

                reply = sendDiscoverCommand(name, optionalServices).get();

                if (reply.getMessage() != null) {
                    System.out.println("an error occurred while trying to discover.");
                    System.out.println("error: " + reply.getErrorMessage());
                }
                System.out.println("trying to connect to device.");
                connectionLock.await();
                reply = sendReadJsonCommand(jsonConnectPeripheral()).get(); //success if response null
                if (reply.getMessage() != null) {
                    System.out.println("an error occurred while trying to connect to device.");
                    System.out.println("error: " + reply.getErrorMessage());
                }
                //wait for connection to be established
                System.out.println("connected to device " + name);

            }
        }catch (InterruptedException | ExecutionException e){
            e.printStackTrace();
            System.out.println("error: connection failed to be established.");
        }
    }

    //easy to use alias for connecting to all services
    public void connectToDevice(String name) {
        connectToDeviceAllServices(name);
    }


    private JsonObject jsonConnectPeripheral() {
        return jsonConnectPeripheral(lastFoundPeripheralId);
    }

    private JsonObject jsonConnectPeripheral(String peripheralId) {
        return jsonRPCBase("connect", "connection")
                .add("params", Json.createObjectBuilder()
                        .add("peripheralId", peripheralId))
                .build();
    }

    private JsonObject jsonGetServices() {
        return jsonRPCBase("getServices")
                .add("params", Json.createObjectBuilder())
                .build();
    }

    public Future<DeviceCommunicatedDataBLE> sendReadMessage(String serviceId, String characteristicId) {
        return sendReadJsonCommand(jsonReadCharacteristic(serviceId, characteristicId));
    }

    public String[] getServices() {
        try {
            return sendReadJsonCommand(jsonGetServices()).get().getMessageArray();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    private Future<DeviceCommunicatedDataBLE> sendReadJsonCommand(JsonObject command) {
        return sendReadCommand(command.toString(), command.getString("id"));
    }

    private Future<DeviceCommunicatedDataBLE> sendReadCommand(String command, String id){
        DeviceCommunicatedDataBLE idData = new DeviceCommunicatedDataBLE();
        synchronized (idData) {
            CommunicationDataMap.put(id, idData);
        }
        return readDeviceExecutor.submit(() -> {
                    send(command);
                    //wait for a notify that is called when idData is updated
                    synchronized (idData) {
                        try {
                            idData.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    return idData;
                }
        );
    }

    public void sendWriteCommand(String serviceId, String characteristicId, byte[] data) {
        send(jsonWriteCharacteristic(serviceId, characteristicId, data).toString());
    }

    public void sendStringWriteCommand(String serviceId, String characteristicId, String data) {
        send(jsonStringWriteCharacteristic(serviceId, characteristicId, data).toString());
    }

    public void sendStartNotificationsCommand(String serviceId, String characteristicId) {
        send(jsonStartNotifications(serviceId, characteristicId).toString());
    }

    public void sendStopNotificationsCommand(String serviceId, String characteristicId) {
        send(jsonStopNotifications(serviceId, characteristicId).toString());
    }

    public void sendNotificationsStateCommand(String serviceId, String characteristicId, boolean state) {
        if (state) {
            sendStartNotificationsCommand(serviceId, characteristicId);
        } else {
            sendStopNotificationsCommand(serviceId, characteristicId);
        }
    }

    @Override
    public BluetoothCommunicationInterface getInstance() {
        return this;
    }

    private JsonObject jsonReadCharacteristic(String serviceId, String characteristicId) {
        return jsonRPCBase("read")
                .add("params", Json.createObjectBuilder()
                        .add("serviceId", serviceId)
                        .add("characteristicId", characteristicId))
                .build();
    }

    private JsonObject jsonStartNotifications(String serviceId, String characteristicId) {
        return jsonRPCBase("startNotifications")
                .add("params", Json.createObjectBuilder()
                        .add("serviceId", serviceId)
                        .add("characteristicId", characteristicId)
                        .add("encoding", "base64"))
                .build();
    }

    private JsonObject jsonStopNotifications(String serviceId, String characteristicId) {
        return jsonRPCBase("stopNotifications")
                .add("params", Json.createObjectBuilder()
                        .add("serviceId", serviceId)
                        .add("characteristicId", characteristicId))
                .build();
    }

    private JsonObject jsonWriteCharacteristic(String serviceId, String characteristicId, byte[] data) {
        return jsonWriteCharacteristic(serviceId, characteristicId, Util.BytesToStringBase64(data), false);
    }

    private JsonObject jsonStringWriteCharacteristic(String serviceId, String characteristicId, String data) {
        return jsonWriteCharacteristic(serviceId, characteristicId, data, true);
    }

    private JsonObject jsonWriteCharacteristic(String serviceId, String characteristicId, String data, boolean asString) {
        JsonObjectBuilder params = Json.createObjectBuilder()
                .add("serviceId", serviceId)
                .add("characteristicId", characteristicId)
                .add("message", data);
        if (!asString) {
            params.add("encoding", "base64");
        }
        params.add("withResponse", true);
        return jsonRPCBase("write").add("params", params).build();
    }


    private void shutdown() {
        close();
        readDeviceExecutor.shutdown();
    }
}
