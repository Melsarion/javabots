package Communication;

import java.util.concurrent.Future;

public interface BluetoothCommunicationInterface {
    void start();
    void stop();
    void connectToDevice(String name);
    Future<DeviceCommunicatedDataBLE> sendReadMessage(String serviceId, String characteristicId);
    String[] getServices();
    void sendWriteCommand(String serviceId, String characteristicId, byte[] data);
    void sendStopNotificationsCommand(String serviceId, String characteristicId);
    void sendStartNotificationsCommand(String serviceId, String characteristicId);
    void sendNotificationsStateCommand(String serviceId, String characteristicId, boolean state);
    BluetoothCommunicationInterface getInstance();


}
