import MBot.*;
import MBot.Listeners.ButtonListener;

public class MbotLinefollower extends MbotDevice implements ButtonListener {
    private Motors motors = new Motors();
    private Button button = new Button();
    private LineFollowSensor sensor;
    private volatile boolean followLine = true;
    public MbotLinefollower(int portLineFollowSensor) {
        sensor = new LineFollowSensor(portLineFollowSensor);
        addPart(sensor, button, motors);
        button.addButtonListener(this);
    }

    @Override
    public void onButtonPress() {
        followLine = false;
    }

    public void startLineFollow(){
        while(followLine) {
            LineFollowerState state = sensor.readLineFollowSensor();
            if (state.noneSensors()) {
                //move straight
                motors.setMotors(140);
            } else if (state.lefSensor() && !state.rightSensor()) {
                //turn right
                motors.setMotor(Motor.LEFT, 110);
                motors.setMotor(Motor.RIGHT, 0);
            } else if (!state.lefSensor() && state.rightSensor()) {
                //turn left
                motors.setMotor(Motor.LEFT, 0);
                motors.setMotor(Motor.RIGHT, 110);
            }
        }
        motors.setMotors(0);
    }

    public static void main(String[] args) {
        MbotLinefollower mbot = new MbotLinefollower(2);
        mbot.start();
        mbot.startLineFollow();
    }
}
