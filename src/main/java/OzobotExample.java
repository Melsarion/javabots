import OzobotEvo.Ozobot;

import static java.lang.Thread.sleep;

public class OzobotExample extends Ozobot {

        public void flashLights() throws InterruptedException {
            setLED(10,200,200,200);
            sleep(500);
            setLED(10,0,0,0);
        }
    public static void main(String[] args) throws InterruptedException {
        OzobotExample ozobot = new OzobotExample();
        ozobot.start();
        ozobot.flashLights();
        sleep(500);
        ozobot.flashLights();
    }
}
