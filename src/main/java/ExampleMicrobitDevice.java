import Microbit.Button;
import Microbit.LedMatrix;
import Microbit.Listeners.ButtonListener;
import Microbit.MicrobitDevice;
import Microbit.StateLed;

import java.util.Arrays;

public class ExampleMicrobitDevice extends MicrobitDevice implements ButtonListener {
    private int counter = 0;

    public ExampleMicrobitDevice() {
        getButtons().addButtonListener(this);
    }


    public void onButtonReleased(Button buttonName) {
        if (buttonName == Button.A) {
            onButtonA();
        } else {
            onButtonB();
        }
        checkCounter();
    }

    private void onButtonA() {
        getLedMatrix().setLed(counter, StateLed.ON);
        counter++;

    }

    private void onButtonB() {
        StateLed[] ledsOff = new StateLed[LedMatrix.amountOfLeds];
        //fill all values with OFF
        Arrays.fill(ledsOff, StateLed.OFF);
        getLedMatrix().setLedMatrix(ledsOff);
        counter = 0;
    }

    private void checkCounter() {
        if (counter == LedMatrix.amountOfLeds) {
            getLedMatrix().showScrollingText("Good job!");
            counter = 0;
        }
    }

    public static void main(String[] args) {
        new ExampleMicrobitDevice().start("vuzot");
    }
}
