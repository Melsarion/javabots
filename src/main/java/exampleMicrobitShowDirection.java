import Microbit.Listeners.AccelerometerListener;
import Microbit.MicrobitDevice;
import Microbit.StateLed;

import javax.vecmath.Vector3d;

public class exampleMicrobitShowDirection extends MicrobitDevice implements AccelerometerListener {
    public exampleMicrobitShowDirection() {
        getAccelerometer().addAccelerometerListener(this);
    }

    @Override
    public void onAccelerometerChange(Vector3d newValue) {
        //absolute values of the acceleration
        double absX = Math.abs(newValue.x);
        double absY = Math.abs(newValue.y);
        double absZ = Math.abs(newValue.z);
        //check if X is biggest value
        if(absX > absY && absX > absZ){
            //check if positive or negative acceleration
            if (newValue.x > 0){
                showArrowLeft();
            }else{
                showArrowRight();
            }
        //check if Y is biggest value
        }else if(absY > absX && absY > absZ){
            if (newValue.y > 0){
                showArrowDown();
            }else{
                showArrowUp();
            }
        //Z is biggest value
        }else{
            showCircle();
        }
    }

    private StateLed o = StateLed.OFF;
    private StateLed x = StateLed.ON;
    public void showCircle(){
        StateLed[] circle = new StateLed[]{
                o,o,o,o,o,
                o,x,x,x,o,
                o,x,o,x,o,
                o,x,x,x,o,
                o,o,o,o,o
        };
        getLedMatrix().setLedMatrix(circle);
    }
    public void showArrowRight(){
        StateLed[] arrowRight = new StateLed[]{
                o,o,x,o,o,
                o,x,o,o,o,
                x,x,x,x,x,
                o,x,o,o,o,
                o,o,x,o,o
        };
        getLedMatrix().setLedMatrix(arrowRight);
    }
    public void showArrowLeft(){
        StateLed[] arrowLeft = new StateLed[]{
                o,o,x,o,o,
                o,o,o,x,o,
                x,x,x,x,x,
                o,o,o,x,o,
                o,o,x,o,o
        };
        getLedMatrix().setLedMatrix(arrowLeft);
    }
    public void showArrowUp(){
        StateLed[] arrowUp = new StateLed[]{
                o,o,x,o,o,
                o,x,x,x,o,
                x,o,x,o,x,
                o,o,x,o,o,
                o,o,x,o,o
        };
        getLedMatrix().setLedMatrix(arrowUp);
    }
    public void showArrowDown(){
        StateLed[] arrowDown = new StateLed[]{
                o,o,x,o,o,
                o,o,x,o,o,
                x,o,x,o,x,
                o,x,x,x,o,
                o,o,x,o,o
        };
        getLedMatrix().setLedMatrix(arrowDown);
    }

    public static void main(String[] args) {
        new exampleMicrobitShowDirection().start("vazat");
    }
}
