package Microbit;

import Microbit.Listeners.PinListener;

import java.util.ArrayList;

class Pin {
    //every pin has digital capability
    private boolean analogCapability;
    private StateIO stateIO;
    private StateAD stateAD;
    private int pinNumber;
    private int value;
    private Pins pinBoard;
    private ArrayList<PinListener> listeners = new ArrayList<>();

    public Pin(int pinNumber, boolean analogCapability, StateIO stateIO, StateAD stateAD, Pins pinBoard) {
        this.analogCapability = analogCapability;
        this.stateIO = stateIO;
        this.stateAD = stateAD;
        this.pinNumber = pinNumber;
        this.pinBoard = pinBoard;
    }

    StateIO getStateIO() {
        return stateIO;
    }

    public void setStateIO(StateIO stateIO) {
        this.stateIO = stateIO;
        pinBoard.updatePinConfigIO();
    }

    StateAD getStateAD() {
        return stateAD;
    }

    public void setStateAD(StateAD stateAD) {
        this.stateAD = stateAD;
        pinBoard.updatePinConfigAD();
    }

    int getValue() {
        pinBoard.updateInputValues();
        return value;
    }

    void setValue(int value) {
        this.value = value;
        pinBoard.updateOutputValue(pinNumber, value);
    }

    public void setPinPWM(int value, int period) {
        pinBoard.setPinPWM(pinNumber, value, period);
    }

    public int getAnalogValue() {
        if (isAnalog() && isInput()) {
            return getValue();
        } else {
            throw new IllegalGPIOStateException("Error: pin number " + pinNumber + ". " + (!isAnalog() ? ("Pin is not set as Analog but an analog value was requested.") : "")
                    + (!isInput() ? ("Pin is not set as an input pin.") : "") + " This is an illegal read request.");
        }
    }

    public void setAnalogValue(int value) {
        if (isAnalog() && isOutput() && (value > 0 && value < 1024)) {
            setValue(value);
        } else {
            throw new IllegalGPIOStateException("Error: pin number " + pinNumber + ". "
                    + (!isAnalog() ? ("Pin is not set as Analog but an analog value was written. ") : "")
                    + (!isOutput() ? ("Pin is not set as an output pin.") : "")
                    + (!(value > 0 && value < 1024) ? "Invalid value received, which should be between 0 and 1024. Value received: " + value : "")
                    + "This is an illegal write request.");
        }
    }

    public int getDigitalValue() {
        if (isDigital() && isInput()) {
            return getValue();
        } else {
            throw new IllegalGPIOStateException("Error: pin number " + pinNumber + ". " + (!isDigital() ? ("Pin is not set as Digital but a digital value was requested.") : "")
                    + (!isInput() ? ("Pin is not set as an input pin.") : "") + " This is an illegal read request.");
        }
    }

    public void setDigitalValue(int value) {
        if (isDigital() && isOutput() && (value == 0 || value == 1)) {
            setValue(value);
        } else {
            throw new IllegalGPIOStateException("Error: pin number " + pinNumber + ". "
                    + (!isDigital() ? ("Pin is not set as Digital but a digital value was written. ") : "")
                    + (!isOutput() ? ("Pin is not set as an output pin.") : "")
                    + (!(value > 0 && value < 1024) ? "Invalid value received, which should be 0 or 1. Value received: " + value : "")
                    + "This is an illegal write request.");
        }
    }

    public boolean isDigital() {
        return stateAD == StateAD.DIGITAL;
    }

    public boolean isAnalog() {
        return stateAD == StateAD.ANALOG;
    }

    public boolean isInput() {
        return stateIO == StateIO.INPUT;
    }

    public boolean isOutput() {
        return stateIO == StateIO.OUTPUT;
    }

    public void addPinListener(PinListener listener) {
        listeners.add(listener);
    }

    public void removePinListener(PinListener listener) {
        listeners.remove(listener);
    }

    void onPinChange(int value) {
        for (PinListener listener : listeners) {
            listener.onPinChange(value, getStateAD());
        }
    }

}
