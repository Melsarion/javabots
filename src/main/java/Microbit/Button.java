package Microbit;

/**
 * enum for both buttons in the micro:bit.
 */
public enum Button {
    A,B
}
