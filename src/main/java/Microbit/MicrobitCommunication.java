package Microbit;

import Communication.*;

import javax.vecmath.Vector3d;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MicrobitCommunication implements BluetoothParseInterface, MicrobitCommunicationInterface {
    interface NotificationCommand {
        void runCommand(byte[] bytes);
    }

    private final String ACCELEROMETERSERVICE_SERVICE_UUID = Util.normalizeUUID("E95D0753251D470AA062FA1922DFA9A8");
    private final String ACCELEROMETERDATA_CHARACTERISTIC_UUID = Util.normalizeUUID("E95DCA4B251D470AA062FA1922DFA9A8");
    private final String ACCELEROMETERPERIOD_CHARACTERISTIC_UUID = Util.normalizeUUID("E95DFB24251D470AA062FA1922DFA9A8");

    private final String BUTTONSERVICE_SERVICE_UUID = Util.normalizeUUID("E95D9882251D470AA062FA1922DFA9A8");
    private final String BUTTON1STATE_CHARACTERISTIC_UUID = Util.normalizeUUID("E95DDA90251D470AA062FA1922DFA9A8");
    private final String BUTTON2STATE_CHARACTERISTIC_UUID = Util.normalizeUUID("E95DDA91251D470AA062FA1922DFA9A8");

    private final String IOPINSERVICE_SERVICE_UUID = Util.normalizeUUID("E95D127B251D470AA062FA1922DFA9A8");
    private final String PINDATA_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D8D00251D470AA062FA1922DFA9A8");
    private final String PINADCONFIGURATION_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D5899251D470AA062FA1922DFA9A8");
    private final String PINIOCONFIGURATION_CHARACTERISTIC_UUID = Util.normalizeUUID("E95DB9FE251D470AA062FA1922DFA9A8");
    private final String PINPWMCONFIGURATION_CHARACTERISTIC_UUID = Util.normalizeUUID("E95DD822251D470AA062FA1922DFA9A8");

    private final String LEDSERVICE_SERVICE_UUID = Util.normalizeUUID("E95DD91D251D470AA062FA1922DFA9A8");
    private final String LEDMATRIXSTATE_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D7B77251D470AA062FA1922DFA9A8");
    private final String LEDTEXT_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D93EE251D470AA062FA1922DFA9A8");
    private final String LEDSCROLLINGDELAY_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D0D2D251D470AA062FA1922DFA9A8");

    private final String MAGNETOMETERSERVICE_SERVICE_UUID = Util.normalizeUUID("E95DF2D8251D470AA062FA1922DFA9A8");
    private final String MAGNETOMETERDATA_CHARACTERISTIC_UUID = Util.normalizeUUID("E95DFB11251D470AA062FA1922DFA9A8");
    private final String MAGNETOMETERPERIOD_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D386C251D470AA062FA1922DFA9A8");
    private final String MAGNETOMETERBEARING_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D9715251D470AA062FA1922DFA9A8");
    private final String MAGNETOMETERCALIBRATION_CHARACTERISTIC_UUID = Util.normalizeUUID("E95DB358251D470AA062FA1922DFA9A8");

    private final String TEMPERATURESERVICE_SERVICE_UUID = Util.normalizeUUID("E95D6100251D470AA062FA1922DFA9A8");
    private final String TEMPERATURE_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D9250251D470AA062FA1922DFA9A8");
    private final String TEMPERATUREPERIOD_CHARACTERISTIC_UUID = Util.normalizeUUID("E95D1B25251D470AA062FA1922DFA9A8");

    private String deviceName = null;
    private BluetoothCommunicationInterface bluetoothService;
    private Map<String, NotificationCommand> notificationCommands = new HashMap<>();
    private MicrobitDeviceInterface device;

    private Vector3d vectorAccelerometer = null;
    private Vector3d vectorMagnetometer = null;
    private byte[] ledMatrixCache = null;

    public MicrobitCommunication(String name) {
        this();
        this.setName(name);
    }

    private MicrobitCommunication() {
        bluetoothService = new BluetoothCommunication(this);
        device = null;
    }

    void setName(String name) {
        if (name.length() == 5) {
            //short name was given
            deviceName = "BBC micro:bit [" + name + "]";
        } else {
            deviceName = name;
        }
    }

    public MicrobitCommunication(String name, MicrobitDeviceInterface device) throws URISyntaxException {
        this(name);
        this.device = device;
    }
    public MicrobitCommunication(MicrobitDeviceInterface device) {
        this();
        this.device = device;
    }

    public void start(String name) {
        setName(name);
        start();
    }

    public void start() {
        if (deviceName == null) {
            System.out.println("error: can not start device. Device name was not set. Use start(deviceName) to set the name of the device.");
            return;
        }
        bluetoothService.start();
        String[] desiredServices = new String[]{ACCELEROMETERSERVICE_SERVICE_UUID, BUTTONSERVICE_SERVICE_UUID, IOPINSERVICE_SERVICE_UUID, LEDSERVICE_SERVICE_UUID, MAGNETOMETERSERVICE_SERVICE_UUID, TEMPERATURESERVICE_SERVICE_UUID};
        //no magneto
        String[] desiredServicesNoMagneto = new String[]{ACCELEROMETERSERVICE_SERVICE_UUID, BUTTONSERVICE_SERVICE_UUID, IOPINSERVICE_SERVICE_UUID, LEDSERVICE_SERVICE_UUID, TEMPERATURESERVICE_SERVICE_UUID};
        //casting for quicker connection if Scratch link on Windows or Mac is used
        if(bluetoothService.getInstance() instanceof BluetoothCommunicationScratchLink){
            ((BluetoothCommunicationScratchLink) bluetoothService.getInstance()).connectToDevice(deviceName,desiredServices);
        }else {
            bluetoothService.connectToDevice(deviceName);
        }
        //check if all needed services are present. If not, wrong software is probably on the micro:bit
        String[] availableServices = bluetoothService.getServices();
        for (String service : desiredServicesNoMagneto) {//firmware without magnetometer is also allowed
            if (Arrays.stream(availableServices).noneMatch(service::equals)) {
                throw new InvalidFirmwareException("Micro:bit does not contain service " + service + ". " +
                "Micro:bit contains the wrong firmware. Upload the correct HEX file to the micro:bit and try again.");
            }
        }
    }

    @Override
    public void stop() {
        bluetoothService.stop();
    }

    private void setNotificationState(String serviceId, String characteristicId, NotificationCommand function, boolean state) {
        if (state) {
            notificationCommands.put(characteristicId, function);
        } else {
            notificationCommands.remove(characteristicId);
        }
        bluetoothService.sendNotificationsStateCommand(serviceId, characteristicId, state);
    }

    //return X Y Z
    public Vector3d readAccelerometer() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(ACCELEROMETERSERVICE_SERVICE_UUID, ACCELEROMETERDATA_CHARACTERISTIC_UUID);
        byte[] bytes = idData.getByteMessage();
        Vector3d vector = Util.vector3dFromShortLittleEndianBytes(bytes);
        vector.scale(1.0 / 1000.0);
        return vector;
    }

    private void onAccelerometerNotification(byte[] data) {
        Vector3d vector = Util.vector3dFromShortLittleEndianBytes(data);
        vector.scale(1.0 / 1000.0);
        vectorAccelerometer = Util.lowPassFilter(vector,vectorAccelerometer);
        device.onDeviceAccelerometerChange(vectorAccelerometer);
    }

    public void setAccelerometerNotificationsState(boolean state) {
        setNotificationState(ACCELEROMETERSERVICE_SERVICE_UUID, ACCELEROMETERDATA_CHARACTERISTIC_UUID, this::onAccelerometerNotification, state);
    }

    public void setAccelerometerPollingPeriod(int periodInMs) {
        if (periodInMs == 1 || periodInMs == 2 || periodInMs == 5 || periodInMs == 10 || periodInMs == 20 || periodInMs == 80 || periodInMs == 160 || periodInMs == 640) {
            sendCommandToDevice(ACCELEROMETERSERVICE_SERVICE_UUID, ACCELEROMETERPERIOD_CHARACTERISTIC_UUID, Util.bytesLittleEndianFromShort((short) periodInMs));
        } else {
            System.out.println("error: wrong polling period");
        }
    }

    public int readAccelerometerPollingPeriod() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(ACCELEROMETERSERVICE_SERVICE_UUID, ACCELEROMETERDATA_CHARACTERISTIC_UUID);
        return Util.shortFromLittleEndianBytes(idData.getByteMessage());
    }

    private void onButton1Notification(byte[] state) {
        onButtonNotification(Button.A, state);
    }

    private void onButton2Notification(byte[] state) {
        onButtonNotification(Button.B, state);
    }

    private void onButtonNotification(Button buttonName, byte[] state) {

        switch (state[0]) {
            case (byte) 0x00:
                device.onDeviceButtonReleased(buttonName);
            case (byte) 0x01:
                device.onDeviceButtonPressed(buttonName);
            case (byte) 0x02:
                device.onDeviceButtonLongPress(buttonName);
        }
    }
    public void setButtonsNotificationsState(boolean state){
        setButton1NotificationsState(state);
        setButton2NotificationsState(state);
    }
    private void setButton1NotificationsState(boolean state) {
        setNotificationState(BUTTONSERVICE_SERVICE_UUID, BUTTON1STATE_CHARACTERISTIC_UUID, this::onButton1Notification, state);
    }

    private void setButton2NotificationsState(boolean state) {
        setNotificationState(BUTTONSERVICE_SERVICE_UUID, BUTTON2STATE_CHARACTERISTIC_UUID, this::onButton2Notification, state);
    }

    private void onPinNotification(byte[] state) {
        for (int i = 0; i < state.length; i += 2) {
            device.onDevicePinChange(state[i], state[i + 1]);
        }
    }

    //states as pairs of pin_number and state. e.g. 0x00 0x01 to turn on pin 0
    //ignored if pin was not configured as output first
    public void setPinState(byte[] pinStates) {
        sendCommandToDevice(IOPINSERVICE_SERVICE_UUID, PINDATA_CHARACTERISTIC_UUID, pinStates);
    }

    public void setPinNotifications(boolean state) {
        setNotificationState(IOPINSERVICE_SERVICE_UUID, PINDATA_CHARACTERISTIC_UUID, this::onPinNotification, state);
    }

    //input states. Pairs are given as [pinNumber, pinState, ...] for only input pins
    public byte[] readInputPinsState() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(IOPINSERVICE_SERVICE_UUID, PINDATA_CHARACTERISTIC_UUID);
        return idData.getByteMessage();
    }

    //array of states (true=analog or false=digital), max length 19 (=amount of pins)
    //1 is analog, 0 is digital
    public void setPinADConfigurationMask(int bitmask) {
        sendCommandToDevice(IOPINSERVICE_SERVICE_UUID, PINADCONFIGURATION_CHARACTERISTIC_UUID, Util.bytesLittleEndianFromInt(bitmask));
    }

    public int readPinADConfigurationMask() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(IOPINSERVICE_SERVICE_UUID, PINADCONFIGURATION_CHARACTERISTIC_UUID);
        return Util.intFromLittleEndianBytes(idData.getByteMessage());
    }

    //0 is an output pin, 1 is input pin (data will be read)
    public void setPinIOConfigurationMask(int bitmask) {
        //send bitmask where 0 is digital, 1 is analog
        sendCommandToDevice(IOPINSERVICE_SERVICE_UUID, PINIOCONFIGURATION_CHARACTERISTIC_UUID, Util.bytesLittleEndianFromInt(bitmask));
    }

    public int readPinIOConfigurationMask() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(IOPINSERVICE_SERVICE_UUID, PINIOCONFIGURATION_CHARACTERISTIC_UUID);
        return Util.intFromLittleEndianBytes(idData.getByteMessage());
    }

    //can't be read
    /*Period is in microseconds and is an unsigned int but transmitted.
Value is in the range 0 – 1024, per the current DAL API (e.g. setAnalogValue). 0 means OFF.*/
    public void setPinPWMConfiguration(int pin, int value, int period) {
        byte[] config = new byte[7];
        config[0] = (byte) pin;
        System.arraycopy(Util.bytesLittleEndianFromShort((short) value), 0, config, 1, 2);
        System.arraycopy(Util.bytesLittleEndianFromInt(period), 0, config, 3, 4);
        sendCommandToDevice(IOPINSERVICE_SERVICE_UUID, PINPWMCONFIGURATION_CHARACTERISTIC_UUID, config);
    }

    public void setLedMatrix(byte[] ledMatrix) {
        if(!Arrays.equals(ledMatrix,ledMatrixCache)) {
            ledMatrixCache = ledMatrix;
            sendCommandToDevice(LEDSERVICE_SERVICE_UUID, LEDMATRIXSTATE_CHARACTERISTIC_UUID, ledMatrix);
        }
    }

    public byte[] readLedMatrix() {
        /*if(ledMatrixCache != null){
            return ledMatrixCache;
        }else {*/
        DeviceCommunicatedDataBLE idData = readCharacteristic(LEDSERVICE_SERVICE_UUID, LEDMATRIXSTATE_CHARACTERISTIC_UUID);
        ledMatrixCache = idData.getByteMessage();
        return idData.getByteMessage();
        //}
    }

    public void showLedText(String text) {
        if (text.length() <= 20) {
            byte[] textEncoded = Util.textToBytesUTF8(text);
            bluetoothService.sendWriteCommand(LEDSERVICE_SERVICE_UUID, LEDTEXT_CHARACTERISTIC_UUID, textEncoded);
        } else {
            System.out.println("Maximum length for text is 20.");
        }
    }

    public void setLedScrollingDelay(short delayInMs) {
        sendCommandToDevice(LEDSERVICE_SERVICE_UUID, LEDSCROLLINGDELAY_CHARACTERISTIC_UUID, Util.bytesLittleEndianFromShort(delayInMs));
    }

    public int readLedScrollingDelay() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(IOPINSERVICE_SERVICE_UUID, PINADCONFIGURATION_CHARACTERISTIC_UUID);
        return Util.shortFromLittleEndianBytes(idData.getByteMessage());
    }

    //returns signed 8 bit value
    public int readTemperature() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(TEMPERATURESERVICE_SERVICE_UUID, TEMPERATURE_CHARACTERISTIC_UUID);
        return Util.intFromLittleEndianBytes(idData.getByteMessage());
    }

    public void setTemperatureNotifications(boolean state) {
        setNotificationState(TEMPERATURESERVICE_SERVICE_UUID, TEMPERATURE_CHARACTERISTIC_UUID, this::onTemperatureNotification, state);
    }

    private void onTemperatureNotification(byte[] bytes) {
        device.onDeviceTemperatureChange(bytes[0]);
    }

    public void setTemperatureUpdateFrequency(int delayInMs) {
        sendCommandToDevice(TEMPERATURESERVICE_SERVICE_UUID, TEMPERATUREPERIOD_CHARACTERISTIC_UUID, Util.bytesLittleEndianFromShort((short) delayInMs));
    }

    public int readTemperatureUpdateFrequency() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(TEMPERATURESERVICE_SERVICE_UUID, TEMPERATUREPERIOD_CHARACTERISTIC_UUID);
        return Util.shortFromLittleEndianBytes(idData.getByteMessage());
    }

    public Vector3d readMagnetometer() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(MAGNETOMETERSERVICE_SERVICE_UUID, MAGNETOMETERDATA_CHARACTERISTIC_UUID);
        byte[] bytes = idData.getByteMessage();
        Vector3d vector = Util.vector3dFromShortLittleEndianBytes(bytes);
        vector.scale(1.0/1000.0);
        return vector;
    }

    private void onMagnetometerNotification(byte[] data) {
        Vector3d vector = Util.vector3dFromShortLittleEndianBytes(data);
        vector.scale(1.0/1000.0);
        vectorMagnetometer = Util.lowPassFilter(vector,vectorMagnetometer);
        device.onDeviceMagnetometerChange(vectorMagnetometer);
    }

    public void setMagnetometerNotificationState(boolean state) {
        setNotificationState(MAGNETOMETERSERVICE_SERVICE_UUID, MAGNETOMETERDATA_CHARACTERISTIC_UUID, this::onMagnetometerNotification, state);
        setMagnetometerBearingNotificationState(state);
    }

    public void setMagnetometerPollingPeriod(int periodInMs) {
        if (periodInMs == 1 || periodInMs == 2 || periodInMs == 5 || periodInMs == 10 || periodInMs == 20 || periodInMs == 80 || periodInMs == 160 || periodInMs == 640) {
            sendCommandToDevice(MAGNETOMETERSERVICE_SERVICE_UUID, MAGNETOMETERPERIOD_CHARACTERISTIC_UUID, Util.bytesLittleEndianFromShort((short) periodInMs));
        } else {
            System.out.println("error: wrong polling period");
        }
    }

    public int readMagnetometerPollingPeriod() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(MAGNETOMETERSERVICE_SERVICE_UUID, MAGNETOMETERPERIOD_CHARACTERISTIC_UUID);
        return Util.shortFromLittleEndianBytes(idData.getByteMessage());
    }

    //16bit int in degrees
    public int readMagnetometerBearing() {
        DeviceCommunicatedDataBLE idData = readCharacteristic(MAGNETOMETERSERVICE_SERVICE_UUID, MAGNETOMETERBEARING_CHARACTERISTIC_UUID);
        return Util.shortFromLittleEndianBytes(idData.getByteMessage());
    }

    private void onMagnetometerBearingNotification(byte[] data) {
        device.onDeviceMagnetometerBearingChange(Util.shortFromLittleEndianBytes(data));
    }

    private void setMagnetometerBearingNotificationState(boolean state) {
        setNotificationState(MAGNETOMETERSERVICE_SERVICE_UUID, MAGNETOMETERBEARING_CHARACTERISTIC_UUID, this::onMagnetometerBearingNotification, state);
    }

    /*0 - state unknown
1 - calibration requested
2 - calibration completed OK
3 - calibration completed with error*/
    public void startMagnetometerCallibration() {
        bluetoothService.sendStartNotificationsCommand(MAGNETOMETERSERVICE_SERVICE_UUID, MAGNETOMETERCALIBRATION_CHARACTERISTIC_UUID);
        sendCommandToDevice(MAGNETOMETERSERVICE_SERVICE_UUID, MAGNETOMETERCALIBRATION_CHARACTERISTIC_UUID, new byte[]{0x01});
        //todo wait for calibration to end? Or add function "calibration done check"?
    }


    private void sendCommandToDevice(String serviceUUID, String characteristicUUID, byte[] payload) {
        bluetoothService.sendWriteCommand(serviceUUID, characteristicUUID, payload);
    }

    private DeviceCommunicatedDataBLE readCharacteristic(String serviceUUID, String characteristicUUID) {
        DeviceCommunicatedDataBLE idData = null;
        try {
            idData = bluetoothService.sendReadMessage(serviceUUID, characteristicUUID).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return idData;
    }

    @Override
    public void parse(DeviceCommunicatedDataBLE idData) {
        if(device != null) {
            //handle notifications by sending them to the device
            if (idData.isByteMessage()) {
                notificationCommands.get(idData.getCharacteristicId()).runCommand(idData.getByteMessage());
            } else {
                System.out.println("non-byte notification received from micro:bit." + idData.getMessage());
            }
        }
    }
}