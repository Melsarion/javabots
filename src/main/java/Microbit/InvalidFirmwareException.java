package Microbit;

public class InvalidFirmwareException extends RuntimeException {
    public InvalidFirmwareException(String message) {
        super(message);
    }
}
