package Microbit.Listeners;

import Microbit.StateAD;

public interface PinListener extends Listener {
    public void onPinChange(int pinValue, StateAD state);
}
