package Microbit.Listeners;

import javax.vecmath.Vector3d;

public interface AccelerometerListener extends Listener{
    public void onAccelerometerChange(Vector3d newValue);
}
