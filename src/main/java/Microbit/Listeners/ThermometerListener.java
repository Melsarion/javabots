package Microbit.Listeners;

public interface ThermometerListener extends Listener {
    public void onTemperatureChange(int temperatureInCelsius);
}
