package Microbit.Listeners;

import javax.vecmath.Vector3d;

public interface MagnetometerListener extends Listener {
     default void onMagnetometerChange(Vector3d newValue){};
     default void onMagnetometerBearingChange(int newBearingFromNorth){};
}
