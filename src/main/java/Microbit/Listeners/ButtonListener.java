package Microbit.Listeners;

import Microbit.Buttons;
import Microbit.Button;
public interface ButtonListener extends Listener{
    public default void onButtonPressed(Button buttonName){};
    public default void onButtonReleased(Button buttonName){};
    public default void onButtonLongPress(Button buttonName){};
}
