package Microbit;

import Device.Device;

import javax.vecmath.Vector3d;

interface MicrobitDeviceInterface {
     void onDeviceAccelerometerChange(Vector3d value);
     void onDeviceMagnetometerChange(Vector3d value);
     void onDeviceMagnetometerBearingChange(short bearingInDegreesNorth);
     void onDeviceButtonPressed(Button buttonName);
     void onDeviceButtonReleased(Button buttonName);
      void onDeviceButtonLongPress(Button buttonName);
     void onDevicePinChange(int pinNumber, byte value);
     void onDeviceTemperatureChange(int tempInCelsius);
}
