package Microbit;

import Device.DevicePart;

// a device that already contains all parts that the micro:bit has
// only parts that are not part of the basic micro:bit are allowed to be added.

/**
 * A micro:bit device containing all the micro:bit standard components: accelerometer, buttons, LED matrix, magnetometer, pins and thermometer.
 * Only external parts still have to be added by calling addPart().
 */
public class MicrobitDevice extends MicrobitBaseDevice {
    // Basic parts of a micro:bit
    private Accelerometer accelerometer = new Accelerometer();
    private Buttons buttons = new Buttons();
    private LedMatrix ledMatrix = new LedMatrix();
    private Magnetometer magnetometer = new Magnetometer();
    private Pins pins = new Pins();
    private Thermometer thermometer = new Thermometer();
    private boolean deviceInitialized = false;

    /**
     * Create a micro:bit device with all standard components, providing the name of the device. Start without a name can be called.
     * @param deviceName Bluetooth name of your micro:bit
     */
    public MicrobitDevice(String deviceName) {
        super(deviceName);
        addBaseParts();
    }

    /**
     * Create a micro:bit device with all standard components. Name has to be provided when calling start.
     */
    public MicrobitDevice() {
        addBaseParts();
    }

    private void addBaseParts(){
        super.addPart(accelerometer, buttons, ledMatrix, magnetometer, pins, thermometer);
        deviceInitialized = true;
    }

    /**
     * Add part to the micro:bit. Only non-standard parts can be added.
     * @param part part to add
     */
    @Override
    protected void addPart(DevicePart part) {
        //only allow adding a non-BasicPart part
        if(!(part instanceof BasicPart && deviceInitialized)) {
            super.addPart(part);
        }else{
            System.out.println("unable to add this part. Use getters to access Basic Parts of the micro:bit.");
        }
    }

    /**
     * Remove part from the micro:bit. Only non-standard parts can be removed.
     * @param part part to remove
     */
    @Override
    protected void removePart(DevicePart part) {
        //only allow removal of non-BasicPart parts
        if(!(part instanceof BasicPart)) {
            super.removePart(part);
        }else{
            System.out.println("unable to remove this part. This part is a module of the micro:bit.");
        }
    }

    /**
     * Access the accelerometer
     * @return accelerometer part
     */
    public Accelerometer getAccelerometer() {
        return accelerometer;
    }

    /**
     * Access the buttons
     * @return buttons part
     */
    public Buttons getButtons() {
        return buttons;
    }

    /**
     * Access the LED matrix
     * @return LED matrix part
     */
    public LedMatrix getLedMatrix() {
        return ledMatrix;
    }

    /**
     * Access the magnetometer
     * @return magnetometer part
     */
    public Magnetometer getMagnetometer() {
        return magnetometer;
    }

    /**
     * Access the pins
     * @return pins part
     */
    public Pins getPins() {
        return pins;
    }

    /**
     * Access the thermometer
     * @return thermometer part
     */
    public Thermometer getThermometer() {
        return thermometer;
    }

}
