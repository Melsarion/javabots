package Microbit;

import Microbit.Listeners.AccelerometerListener;

import javax.vecmath.Vector3d;
import java.util.ArrayList;

/**
 * A micro:bit part for the accelerometer on the micro:bit. It provides read access and listener subscription for the value.
 */
public class Accelerometer extends MicrobitPart implements BasicPart {
    private ArrayList<AccelerometerListener> listeners = new ArrayList<>();

    /**
     * Read the current accelerometer value. This value is updated by a frequency set with setUpdateFrequency()
     *
     * @return Vector3d object with x, y and z values the acceleration per direction. x and y is the horizontal plane.
     */
    public Vector3d readAccelerometer() {
        return getCommunicator(this).readAccelerometer();
    }

    /**
     * Add a listener that will receive a new accelerometer value by a frequency set with setUpdateFrequency()
     *
     * @param listener the accelerometer listener subscribing to updates
     */
    public void addAccelerometerListener(AccelerometerListener listener) {
        listeners.add(listener);
        if(isActive()) {updateNotifications(true);}
    }

    @Override
    protected void activate() {
        super.activate();
        if(listeners.size() != 0){updateNotifications(true);}
    }

    /**
     * Remove a listener. This listener will not receive updates from this part anymore.
     *
     * @param listener the accelerometer listener to remove
     */
    public void removeAccelerometerListener(AccelerometerListener listener) {
        listeners.remove(listener);
    }


    private void updateNotifications(boolean active) {
        getCommunicator(this).setAccelerometerNotificationsState(active);
    }

    void onAccelerometerChange(Vector3d newValue) {
        for (AccelerometerListener listener : listeners) {
            listener.onAccelerometerChange(newValue);
        }
    }

    /**
     * Set the update frequency of the accelerometer. This changes both the polling period for updating the accelerometer value when reading
     * as well as the frequency that an added AccelormeterListener will receive a new value.
     *
     * @param frequencyInMs The frequency in milliseconds to update the accelerometer. Only values 1, 5, 10, 20, 80, 160 and 640 are allowed.
     */
    public void setUpdateFrequency(int frequencyInMs) {
        getCommunicator(this).setAccelerometerPollingPeriod(frequencyInMs);
    }

    /**
     * Get the update frequency of the accelerometer. This is both the polling period for updating the accelerometer value when reading
     * as well as the frequency that an added AccelormeterListener will receive a new value.
     *
     * @return The frequency in milliseconds.
     */
    public int getUpdateFrequency() {
        return getCommunicator(this).readAccelerometerPollingPeriod();
    }

    @Override
    protected void cleanup(boolean isLastPart) {
        if (isLastPart) {
            updateNotifications(false);
        }
    }
}
