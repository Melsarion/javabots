package Microbit;

import Microbit.Listeners.ButtonListener;
import Microbit.Button;

import java.util.ArrayList;

/**
 * A micro:bit part for the buttons attached to the micro:bit. Also called button A and button B.
 */
public class Buttons extends MicrobitPart implements BasicPart {
    private boolean isPressedA = false;
    private boolean isPressedB = false;

    private ArrayList<ButtonListener> listeners = new ArrayList<>();

    /**
     * Add a button listener, which will receive any button event happening on the micro:bit this part is added to.
     * @param listener button listener to add
     */
    public void addButtonListener(ButtonListener listener) {
        listeners.add(listener);
    }

    @Override
    protected void activate() {
        super.activate();
        updateNotifications(true);
    }

    /**
     * Remove a button listener. It will no longer receive updates about button presses of this part.
     * @param listener
     */
    public void removeButtonListener(ButtonListener listener) {
        listeners.remove(listener);
    }

    private void updateNotifications(boolean isActive) {
        getCommunicator(this).setButtonsNotificationsState(isActive);
    }

    private void setButtonState(Button button, boolean state) {
        if (button == Button.A) {
            isPressedA = state;
        } else {
            isPressedB = state;
        }
    }

    private boolean getButtonState(Button button) {
        if (button == Button.A) {
            return isPressedA;
        } else {
            return isPressedB;
        }
    }

    void onButtonPressed(Button buttonName) {
        setButtonState(buttonName, true);
        for (ButtonListener listener : listeners) {
            listener.onButtonPressed(buttonName);
        }
    }

    void onButtonReleased(Button buttonName) {
        setButtonState(buttonName, false);
        for (ButtonListener listener : listeners) {
            listener.onButtonReleased(buttonName);
        }
    }

    void onButtonLongPress(Button buttonName) {
        setButtonState(buttonName, true);
        for (ButtonListener listener : listeners) {
            listener.onButtonLongPress(buttonName);
        }
    }

    /**
     * Check if the button is presses
     * @param buttonName The button to check. This can be Button.A or Button.B
     * @return true if the button is pressed
     */
    public boolean isPressed(Button buttonName) {
        return getButtonState(buttonName);
    }

    /**
     * check if any of the buttons Button.A or Button.B are pressed
     * @return true if one or both buttons are pressed
     */
    //check for any button press
    public boolean isPressed() {
        return getButtonState(Button.A) || getButtonState(Button.B);
    }

    @Override
    protected void cleanup(boolean isLastPart) {
        if (isLastPart) {
            updateNotifications(false);
        }
    }
}
