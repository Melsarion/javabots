package Microbit;

import Communication.Communicator;
import Device.DevicePart;
import Device.Device;

import javax.vecmath.Vector3d;
import java.net.URISyntaxException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * A micro:bit device containing no parts. Add any parts to it you wanna use by calling addPart().
 */
public class MicrobitBaseDevice extends Device implements MicrobitDeviceInterface {
    /*
      the listenerExecutor will execute every listener of this specific device
      all onChange functions will execute in serial
     */
    private final Executor listenerExecutor = Executors.newSingleThreadExecutor();

    private MicrobitCommunication communicator;
    /**
     * Create a micro:bit device without any parts, providing the name of the device. Start() without a name can be called.
     * @param deviceName Bluetooth name of your micro:bit
     */
    public MicrobitBaseDevice(String deviceName) {
        try {
            this.communicator = new MicrobitCommunication(deviceName,this);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Create a micro:bit device without any parts. Name has to be provided when calling start().
     */
    public MicrobitBaseDevice() {
        this.communicator = new MicrobitCommunication(this);
    }

    public MicrobitCommunication getCommunicator(){
        return communicator;
    }

    /**
     * Start the micro:bit device when a name was provided in the constructor.
     * This will start up the Bluetooth connection.
     */
    public void start() {
        communicator.start();
        super.start();
    }

    /**
     * Start the micro:bit device when no name was provided in the constructor or if a different name is desired.
     * This will start up the Bluetooth connection.
     * @param deviceName Bluetooth name of your micro:bit
     */
    public void start(String deviceName){
        communicator.setName(deviceName);
        start();
    }

    @Override
    public void onDeviceAccelerometerChange(Vector3d value) {
        for (DevicePart part : partsList) {
            if (part instanceof Accelerometer) {
                listenerExecutor.execute(() -> ((Accelerometer) part).onAccelerometerChange(value));
            }
        }
    }

    @Override
    public void onDeviceMagnetometerChange(Vector3d value) {
        for (DevicePart part : partsList) {
            if (part instanceof Magnetometer) {
                listenerExecutor.execute(() -> ((Magnetometer) part).onMagnetometerChange(value));
            }
        }
    }

    @Override
    public void onDeviceMagnetometerBearingChange(short bearingInDegreesNorth) {
        for (DevicePart part : partsList) {
            if (part instanceof Magnetometer) {
                listenerExecutor.execute(() -> ((Magnetometer) part).onMagnetometerBearingChange(bearingInDegreesNorth));
            }
        }
    }

    @Override
    public void onDeviceButtonPressed(Button buttonName) {
        for (DevicePart part : partsList) {
            if (part instanceof Buttons) {
                listenerExecutor.execute(() -> ((Buttons) part).onButtonPressed(buttonName));
            }

        }
    }

    @Override
    public void onDeviceButtonReleased(Button buttonName) {
        for (DevicePart part : partsList) {
            if (part instanceof Buttons) {
                listenerExecutor.execute(() -> ((Buttons) part).onButtonReleased(buttonName));
            }

        }
    }

    @Override
    public void onDeviceButtonLongPress(Button buttonName) {
        for (DevicePart part : partsList) {
            if (part instanceof Buttons) {
                listenerExecutor.execute(() -> ((Buttons) part).onButtonLongPress(buttonName));
            }

        }
    }

    @Override
    public void onDevicePinChange(int pinNumber, byte value) {
        for (DevicePart part : partsList) {
            if (part instanceof Pins) {
                listenerExecutor.execute(() -> ((Pins) part).onPinChange(pinNumber, value));
            }
        }
    }

    @Override
    public void onDeviceTemperatureChange(int tempInCelsius) {
        for (DevicePart part : partsList) {
            if (part instanceof Thermometer) {
                listenerExecutor.execute(() -> ((Thermometer) part).onTemperatureChange(tempInCelsius));
            }

        }
    }

}
