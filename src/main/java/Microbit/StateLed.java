package Microbit;

public enum StateLed {
    ON(1),
    OFF(0);
    private int i;

    StateLed(int i) {
        this.i = i;
    }
    public int getOnOffValue() {
        return i;
    }
}
