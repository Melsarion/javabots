package Microbit;

import Communication.Communicator;
import Device.DevicePart;

abstract class MicrobitPart extends DevicePart {
    /**
     * Accessor for communicator of the device bound to this part
     * @param callingPart the device part that asks for Communicator access. Mainly used for error handling.
     * @return the communicator the bound device provides
     */
    protected MicrobitCommunication getCommunicator(DevicePart callingPart) {
        return ((MicrobitDevice) super.getDevice(callingPart)).getCommunicator();
    }
}
