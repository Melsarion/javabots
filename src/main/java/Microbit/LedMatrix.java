package Microbit;

/**
 * A micro:bit part for the LED matrix on the micro:bit. It provides multiple ways to read and write the LED matrix
 * *an array of bytes*: This array is of length 5. Every byte represents one row, where a bit per byte is one light in the row. Note that a byte has 8 bits, so the 3 most significant bits are unused.
 * *an array of States*: This array is of length 25. Only State.ON and State.OFF is allowed. Index 0 is the top left LED and every other index follows the reading direction. For example index 5 is the first LED in the second row, index 24 is the bottom right LED.
 * *an 2D array of States*: This is a 2D array of length 5 x 5. Only State.ON and State.OFF is allowed. Every index is one LED structured as [row, column].
 */
public class LedMatrix extends MicrobitPart implements BasicPart {
    /**
     * the amount of LEDs the micro:bit has on its micro:bit LED matrix
     */
    public static final int amountOfLeds = 25;

    /**
     * Set the LED matrix with an array of bytes.
     * @param matrix an array of bytes with length 5
     */
    public void setLedMatrix(byte[] matrix) {
        if (matrix.length == 5) {
            getCommunicator(this).setLedMatrix(matrix);
        } else {
            throw new IllegalArgumentException("error: length of matrix should be 5. Length is " + matrix.length);
        }
    }

    /**
     * Set the LED matrix with an array of States.
     * @param matrix an array of States of length 25
     */
    public void setLedMatrix(StateLed[] matrix) {
        if (matrix.length == 25) {
            getCommunicator(this).setLedMatrix(ledArrayAsMatrix(matrix));
        } else {
            throw new IllegalArgumentException("error: length of matrix should be 25. Length is " + matrix.length);
        }
    }

    /**
     * Set the LED matrix with a 2D array of States.
     *
     * @param matrix2D a 2D array of States of length 5x5
     */
    public void setLedMatrix(StateLed[][] matrix2D) {
        if (matrix2D.length == 5 && matrix2D[0].length == 5) {
            getCommunicator(this).setLedMatrix(led2DMatrixAsMatrix(matrix2D));
        } else {
            throw new IllegalArgumentException("error: length of matrix should be 5x5. Length is " + matrix2D.length + "x" + matrix2D[0].length);
        }
    }

    /**
     * Read the LED matrix as an array of bytes.
     * @return an array of bytes of length 5
     */
    public byte[] readLedMatrix() {
        return getCommunicator(this).readLedMatrix();
    }

    /**
     * Read the LED matrix as an array of States.
     * @return an array of States of length 25
     */
    public StateLed[] readLedMatrixAsArray() {
        return ledMatrixAsArray(getCommunicator(this).readLedMatrix());
    }

    /**
     * Read the LED matrix as a 2D array of States.
     * @return a 5x5 array of States
     */
    public StateLed[][] readLedMatrixAs2D() {
        return ledMatrixAs2DMatrix(getCommunicator(this).readLedMatrix());
    }

    /**
     * Set the state of one LED
     * @param ledNumber The index of the LED if the LED matrix is set with an array of States.
     * @param state the state to set
     */
    public void setLed(int ledNumber, StateLed state){
        if(ledNumber >= 0 && ledNumber <= 25){
            //todo: synchronize on LedMatrix class?
            synchronized (MicrobitCommunication.class) {
                StateLed[] ledArray = ledMatrixAsArray(getCommunicator(this).readLedMatrix());
                //System.out.println("current led matrix: " + Arrays.toString(ledArray));
                ledArray[ledNumber] = state;
                //System.out.println("new led matrix: " + Arrays.toString(ledArray));
                //System.out.println("set leds as follows: " + Arrays.toString(ledArrayAsMatrix(ledArray)));
                getCommunicator(this).setLedMatrix(ledArrayAsMatrix(ledArray));
            }
        }else{
            throw new IllegalArgumentException("error: ledNumber should be less than 25 and positive. The ledNumber was " + ledNumber);
        }
    }

    /**
     * Set the state of one LED
     * @param ledRow the row of the LED on the LED matrix
     * @param ledColumn the column of the LED on the LED matrix
     * @param state the state to set
     */

    public void setLed(int ledRow, int ledColumn, StateLed state){
        if(ledRow <5 && ledColumn < 5){
            synchronized (MicrobitCommunication.class) {
                StateLed[][] ledMatrix2D = ledMatrixAs2DMatrix(getCommunicator(this).readLedMatrix());
                ledMatrix2D[ledRow][ledColumn] = state;
                getCommunicator(this).setLedMatrix(led2DMatrixAsMatrix(ledMatrix2D));
            }
        }else{
            System.out.println("error: ledNumber should be less than or equal to 25 and positive");
        }
    }

    /**
     * Show a String scrolling across the LED matrix with scrolling speed set by setScrollingTextSpeed(). The maximum length is 20;
     * @param text a string with length 20 or less.
     */
    public void showScrollingText(String text){
        //max length of text is 20
        if(text.length() > 20){
            throw new IllegalArgumentException("error: the length for a scrolling text should be 20 or less. The length is " + text.length() + " for text " + text);
        }
        getCommunicator(this).showLedText(text);
    }

    private StateLed[] ledMatrixAsArray(byte[] matrix) {
        StateLed[] array = new StateLed[25];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                array[i * 5 + j] = (matrix[i] >> (4 - j) & 1)==1?StateLed.ON:StateLed.OFF;
            }
        }
        return array;
    }

    private byte[] ledArrayAsMatrix(StateLed[] ledArray) {
        byte[] matrix = new byte[5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                matrix[i] = (byte) (matrix[i] + ((ledArray[i * 5 + j].getOnOffValue()) << (4 - j)));
            }
        }
        return matrix;
    }

    private StateLed[][] ledMatrixAs2DMatrix(byte[] matrix) {
        StateLed[][] matrix2D = new StateLed[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                matrix2D[i][j] = (matrix[i] >> (4 - j) & 1)==1?StateLed.ON:StateLed.OFF;
            }
        }
        return matrix2D;
    }

    private byte[] led2DMatrixAsMatrix(StateLed[][] matrix2D) {
        byte[] matrix = new byte[5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                matrix[i] = (byte) (matrix[i] + ((byte) (matrix2D[i][j].getOnOffValue()) << (4 - j)));
            }
        }
        return matrix;
    }

    private StateLed[][] ledArrayAs2DMatrix(StateLed[] ledArray) {
        StateLed[][] matrix2D = new StateLed[5][5];
        for (int i = 0; i < 5; i++) {
            System.arraycopy(ledArray, i * 5, matrix2D[i], 0, 4);
        }
        return matrix2D;
    }

    private StateLed[] led2DMatrixAsArray(StateLed[][] matrix2D) {
        StateLed[] array = new StateLed[25];
        for (int i = 0; i < 5; i++) {
            System.arraycopy(matrix2D[i], 0, array, i * 5, 5);
        }
        return array;
    }

    @Override
    protected void cleanup(boolean isLastPart) {
    }
}
