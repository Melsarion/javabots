package Microbit;


import Microbit.Listeners.MagnetometerListener;

import javax.vecmath.Vector3d;
import java.util.ArrayList;

/**
 * A micro:bit part for the magnetometer on the micro:bit. It provides read access and listener subscription for both a representation as a Vector or bearing in degrees from the north.
 */
public class Magnetometer extends MicrobitPart implements BasicPart {
    private ArrayList<MagnetometerListener> listeners = new ArrayList<>();

    /**
     * Read the current magnetometer bearing measurement as a Vector. This value is updated by a frequency set with setUpdateFrequency()
     *
     * @return Vector3d object with x, y and z values the magnetometer bearing measurement per direction. x is the magnetic north, y the magnetic east.
     */
    public Vector3d readMagnetometer() {
        return getCommunicator(this).readMagnetometer();
    }

    /**
     * Read the current magnetometer bearing measurement in degrees from North. This value is updated by a frequency set with setUpdateFrequency()
     *
     * @return int the magnetometer bearing measurement in degrees from North.
     */
    public int readMagnetometerBearing() {
        return getCommunicator(this).readMagnetometerBearing();
    }

    /**
     * Add a listener that will receive a new magnetometer bearing measurement by a frequency set with setUpdateFrequency()
     *
     * @param listener the magnetometer listener subscribing to updates
     */
    public void addMagnetometerListener(MagnetometerListener listener) {
        listeners.add(listener);
        if(isActive()) {updateNotifications(true);}
    }

    @Override
    protected void activate() {
        super.activate();
        if(listeners.size() != 0){updateNotifications(true);}
    }

    /**
     * Remove a listener. This listener will not receive updates from this part anymore.
     *
     * @param listener the magnetometer listener to remove
     */
    public void removeMagnetometerListener(MagnetometerListener listener) {
        listeners.remove(listener);
    }

    /**
     * Set the update frequency of the magnetometer. This changes both the polling period for updating the magnetometer bearing measurement when reading
     * as well as the frequency that an added MagnetometerListener will receive a measurement.
     *
     * @param frequencyInMs The frequency in milliseconds to update the magnetometer. Only values 1, 5, 10, 20, 80, 160 and 640 are allowed.
     */
    public void setUpdateFrequency(int frequencyInMs) {
        getCommunicator(this).setMagnetometerPollingPeriod(frequencyInMs);
    }


    /**
     * Get the update frequency of the magnetometer. This is both the polling period for updating the magnetometer bearing measurement when reading
     * as well as the frequency that an added MagnetometerListener will receive a measurement.
     *
     * @return The frequency in milliseconds.
     */
    public int getUpdateFrequency() {
        return getCommunicator(this).readMagnetometerPollingPeriod();
    }

    private void updateNotifications(boolean isActive) {
        getCommunicator(this).setMagnetometerNotificationState(isActive);
    }

    void onMagnetometerChange(Vector3d newValue) {
        for (MagnetometerListener listener : listeners) {
            listener.onMagnetometerChange(newValue);
        }

    }

    void onMagnetometerBearingChange(int newValue) {
        for (MagnetometerListener listener : listeners) {
            listener.onMagnetometerBearingChange(newValue);
        }
    }

    @Override
    protected void cleanup(boolean isLastPart) {
        if (isLastPart) {
            updateNotifications(false);
        }
    }
}
