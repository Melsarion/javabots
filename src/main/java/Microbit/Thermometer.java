package Microbit;

import Microbit.Listeners.ThermometerListener;

import java.util.ArrayList;

public class Thermometer extends MicrobitPart implements BasicPart{
    private ArrayList<ThermometerListener> listeners = new ArrayList<>();

    /**
     * Read the current temperature in Celcius. This value is updated by a frequency set with setUpdateFrequency()
     * @return int temperature in Celcius
     */
    public int readTemperature() {
        return getCommunicator(this).readTemperature();
    }

    /**
     * Add a listener that will receive a new temperature by a frequency set with setUpdateFrequency()
     * @param listener the thermometer listener subscribing to updates
     */
    public void addThermometerListener(ThermometerListener listener) {
        listeners.add(listener);
        if(isActive()) {updateNotifications(true);}
    }

    @Override
    protected void activate() {
        super.activate();
        if(listeners.size() != 0){updateNotifications(true);}
    }

    /**
     * Remove a listener. This listener will not receive updates from this part anymore.
     * @param listener the thermometer listener to remove
     */
    public void removeThermometerListener(ThermometerListener listener) {
        listeners.remove(listener);
    }

    private void updateNotifications(boolean isActive) {
        getCommunicator(this).setTemperatureNotifications(isActive);
    }

    void onTemperatureChange(int newValue) {
        for (ThermometerListener listener : listeners) {
                listener.onTemperatureChange(newValue);
        }
    }

    /**
     * Get the update frequency of the thermometer. This is both the polling period for updating the temperature when reading
     * as well as the frequency that an added ThermometerListener will receive a new temperature.
     *
     * @return The frequency in milliseconds.
     */
    public int getUpdateFrequency() {
        return getCommunicator(this).readTemperatureUpdateFrequency();
    }

    /**
     * Set the update frequency of the thermometer. This changes both the polling period for updating the temperature when reading
     * as well as the frequency that an added ThermometerListener will receive a new temperature.
     *
     * @param frequency The frequency in milliseconds to update the temperature. Only values 1, 5, 10, 20, 80, 160 and 640 are allowed.
     */
    public void setUpdateFrequency(int frequency) {
        getCommunicator(this).setTemperatureUpdateFrequency(frequency);
    }

    @Override
    protected void cleanup(boolean isLastPart) {
        if(isLastPart) {
            updateNotifications(false);
        }
    }
}
