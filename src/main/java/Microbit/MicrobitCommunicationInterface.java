package Microbit;

import Communication.Communicator;

import javax.vecmath.Vector3d;

public interface MicrobitCommunicationInterface extends Communicator {
    Vector3d readAccelerometer();
    void setAccelerometerNotificationsState(boolean state);
    void setAccelerometerPollingPeriod(int periodInMs);
    int readAccelerometerPollingPeriod();
    void setButtonsNotificationsState(boolean state);
    void setPinState(byte[] pinStates);
    void setPinNotifications(boolean state);
    byte[] readInputPinsState();
    void setPinADConfigurationMask(int bitmask);
    int readPinADConfigurationMask();
    void setPinIOConfigurationMask(int bitmask);
    int readPinIOConfigurationMask();
    void setPinPWMConfiguration(int pin, int value, int period);
    void setLedMatrix(byte[] ledMatrix);
    byte[] readLedMatrix();
    void showLedText(String text);
    void setLedScrollingDelay(short delayInMs);
    int readLedScrollingDelay();
    int readTemperature();
    void setTemperatureNotifications(boolean state);
    void setTemperatureUpdateFrequency(int delayInMs);
    int readTemperatureUpdateFrequency();
    Vector3d readMagnetometer();
    void setMagnetometerNotificationState(boolean state);
    void setMagnetometerPollingPeriod(int periodInMs);
    int readMagnetometerPollingPeriod();
    int readMagnetometerBearing();
}
