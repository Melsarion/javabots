package Microbit;

public class IllegalGPIOStateException extends RuntimeException{
    public IllegalGPIOStateException(String message) {
        super(message);
    }
}
