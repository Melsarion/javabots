package Microbit;

public enum StateAD {
    ANALOG(1),
    DIGITAL(0);
    private int i;

    StateAD(int i) {
        this.i = i;
    }
    public int getAnalogDigitalValue() {
        return i;
    }
}