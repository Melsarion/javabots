package Microbit;

import Communication.Util;

public class Pins extends MicrobitPart implements BasicPart {
    public static final int PINS = 19;

    private Pin[] pinsList = new Pin[19];

    public Pins() {
        for (int pinNumber = 0; pinNumber < PINS; pinNumber++) {
            boolean analogCapability = (pinNumber <= 4 || pinNumber == 10);
            //todo check if base state is indeed digital for all pins (all states output already checked)
            pinsList[pinNumber] = (new Pin(pinNumber, analogCapability, StateIO.OUTPUT, StateAD.DIGITAL, this));
        }
    }

    void updateInputValues() {
        byte[] pinStatePairs = getCommunicator(this).readInputPinsState();
        for (int i = 0; i < pinStatePairs.length; i += 2) {
            pinsList[pinStatePairs[i]].setValue(pinStatePairs[i + 1]);
        }
    }

    void updateOutputValue(int pinNumber, int value) {
        getCommunicator(this).setPinState(new byte[]{(byte) pinNumber, (byte) value});
    }

    void updatePinConfigAD() {
        int bitmask = 0;
        for (int i = 0; i < pinsList.length; i++) {
            bitmask = Util.setBitOfInt(pinsList[i].getStateAD().getAnalogDigitalValue(), bitmask, i);
        }
        getCommunicator(this).setPinADConfigurationMask(bitmask);
    }

    void updatePinConfigIO() {
        int bitmask = 0;
        for (int i = 0; i < pinsList.length; i++) {
            bitmask = Util.setBitOfInt(pinsList[i].getStateIO().getInputOutputValue(), bitmask, i);
        }
        getCommunicator(this).setPinIOConfigurationMask(bitmask);
    }

    public void setPinValue(int pinNumber, int value) {
        pinsList[pinNumber].setValue(value);
    }

    public void setPinAnalog(int pinNumber) {
        pinsList[pinNumber].setStateAD(StateAD.ANALOG);
    }

    public void setPinDigital(int pinNumber) {
        pinsList[pinNumber].setStateAD(StateAD.DIGITAL);
    }

    public void setPinAsInput(int pinNumber) {
        pinsList[pinNumber].setStateIO(StateIO.INPUT);
    }

    public void setPinAsOutput(int pinNumber) {
        pinsList[pinNumber].setStateIO(StateIO.OUTPUT);
    }

    public void setPinPWM(int pinNumber, int value, int period) {
        getCommunicator(this).setPinPWMConfiguration(pinNumber, value, period);
    }

    private void updateNotifications(boolean isActive) {
        getCommunicator(this).setPinNotifications(isActive);
        getCommunicator(this).setPinNotifications(isActive);
    }

    void onPinChange(int pinNumber, int pinValue) {
        pinsList[pinNumber].onPinChange(pinValue);
    }
    public Pin getPin(int pinNumber){
        return pinsList[pinNumber];
    }
    @Override
    protected void activate() {
        super.activate();
        updateNotifications(true);
    }

    @Override
    protected void cleanup(boolean isLastPart) {
        if (isLastPart) {
            updateNotifications(false);
        }
    }
}
