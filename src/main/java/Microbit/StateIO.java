package Microbit;

public enum StateIO {
    INPUT(1),
    OUTPUT(0);
    private int i;

    StateIO(int i) {
        this.i = i;
    }
    public int getInputOutputValue() {
        return i;
    }
}