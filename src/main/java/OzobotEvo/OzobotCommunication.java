package OzobotEvo;

import Communication.*;

public class OzobotCommunication implements OzobotCommunicationInterface {
    private BluetoothCommunicationInterface communicator = new BluetoothCommunication();
    private final static String MOTORSCHARACTERISTIC = "8903136c-5f13-4548-a885-c58779136702";
    private final static String OTHERCHARACTERISTC = "8903136c-5f13-4548-a885-c58779136703";
    private final static String GENERALSERVICE = "8903136c-5f13-4548-a885-c58779136701";
    public void start(){
        communicator.start();
        ((BluetoothCommunicationScratchLink)communicator.getInstance()).connectToDevice("Ozobot Evo",new String[]{GENERALSERVICE});
        //byte message to set ozobot in communication mode
        communicator.sendWriteCommand(GENERALSERVICE,OTHERCHARACTERISTC,new byte[]{0x50,0x02,0x01});
    }

    @Override
    public void setMotors(int leftMotor, int rightMotor, int timeDuration) {
        byte[] command = new byte[7];
        command[0]= 0x40;
        byte[] leftMotorBytes = Util.bytesLittleEndianFromShort((short) leftMotor);
        byte[] rightMotorBytes = Util.bytesLittleEndianFromShort((short) rightMotor);
        byte[] durationBytes = Util.bytesLittleEndianFromShort((short) timeDuration);
        command[1] = leftMotorBytes[0];
        command[2] = leftMotorBytes[1];
        command[3] = rightMotorBytes[0];
        command[4] = rightMotorBytes[1];
        command[5] = durationBytes[0];
        command[6] = durationBytes[1];
        communicator.sendWriteCommand(GENERALSERVICE,MOTORSCHARACTERISTIC,command);
    }

    @Override
    public void setLED(int ledNumber, int R, int G, int B) {
        byte[] command = new byte[6];
        command[0]= 0x44;
        byte[] LEDBytes = Util.bytesLittleEndianFromShort((short) ledNumber);
        command[1] = LEDBytes[0];
        command[2] =LEDBytes[1];
        command[3] = (byte)R;
        command[4] = (byte)G;
        command[5] = (byte)B;
        communicator.sendWriteCommand(GENERALSERVICE,OTHERCHARACTERISTC,command);
    }
}
