package OzobotEvo;

public interface OzobotCommunicationInterface {
    void setMotors(int leftMotor, int rightMotor, int timeDuration);
    void setLED(int ledNumber, int R, int G, int B);
}
